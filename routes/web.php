<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;

Auth::routes();
Route::get('check_email','ResetPasswodCtrl@check_email')->name('check_email');
Route::post('password_update/{id}','ResetPasswodCtrl@password_update')->name('password_update');

Route::middleware(['auth'])->group(function () {
    Route::get('/',function (){
        if(auth()->user()->hasRole('admin')){
            return view('backend.pages.index');
        }
        return view('backend.pages.userDashboard');
    });
    Route::get('/home',function (){
        if(auth()->user()->hasRole('admin')){
            return view('backend.pages.index');
        }
        return view('backend.pages.userDashboard');
    })->name('home');
    //Route::get('/home', 'HomeController@index')->name('home');

    //Routes of Admin and Backend
    Route::namespace('backend')->middleware(['role:admin'])->prefix('admin')->group(function () {

        // Routes of Clients:
        Route::prefix('clients')->group(function (){
            Route::get('all','clientsCtrl@index')->name('admin_clients_all');
            Route::get('create','clientsCtrl@create')->name('admin_clients_create');
            Route::post('store','clientsCtrl@store')->name('admin_clients_store');
            Route::get('show/{slug}','clientsCtrl@show')->name('admin_clients_show');
            Route::get('edit/{slug}','clientsCtrl@edit')->name('admin_clients_edit');
            Route::post('update/{slug}','clientsCtrl@update')->name('admin_clients_update');
            Route::post('destroy/{slug}','clientsCtrl@destroy')->name('admin_clients_destroy');
        });
        // Routes of users:
        Route::prefix('users')->group(function (){
            Route::get('all','usersCtrl@index')->name('admin_users_all');
            Route::get('create','usersCtrl@create')->name('admin_users_create');
            Route::post('store','usersCtrl@store')->name('admin_users_store');
            Route::get('show/{slug}','usersCtrl@show')->name('admin_users_show');
            Route::get('edit/{slug}','usersCtrl@edit')->name('admin_users_edit');
            Route::post('update/{slug}','usersCtrl@update')->name('admin_users_update');
            Route::post('destroy/{slug}','usersCtrl@destroy')->name('admin_users_destroy');
        });
        // Routes of products:
        Route::prefix('products')->group(function (){
            Route::get('all','productsCtrl@index')->name('admin_products_all');
            Route::get('create','productsCtrl@create')->name('admin_products_create');
            Route::post('store','productsCtrl@store')->name('admin_products_store');
            Route::get('show/{slug}','productsCtrl@show')->name('admin_products_show');
            Route::get('edit/{slug}','productsCtrl@edit')->name('admin_products_edit');
            Route::post('update/{slug}','productsCtrl@update')->name('admin_products_update');
            Route::post('destroy/{slug}','productsCtrl@destroy')->name('admin_products_destroy');
        });
        // Routes of categories:
        Route::prefix('categories')->group(function (){
            Route::get('all','categoryCtrl@index')->name('admin_categories_all');
            Route::get('create','categoryCtrl@create')->name('admin_categories_create');
            Route::post('store','categoryCtrl@store')->name('admin_categories_store');
            Route::get('show/{slug}','categoryCtrl@show')->name('admin_categories_show');
            Route::post('update/{slug}','categoryCtrl@update')->name('admin_categories_update');
            Route::post('destroy/{slug}','categoryCtrl@destroy')->name('admin_categories_destroy');
        });
        // Routes of itineraries:
        Route::prefix('itineraries')->group(function (){
            Route::get('all','itinerariesCtrl@index')->name('admin_itineraries_all');
            Route::get('show/{slug}','itinerariesCtrl@show')->name('admin_itineraries_show');
        });
        // Routes of reports:
        Route::prefix('reports')->group(function (){
            Route::get('create','reportsCtrl@create')->name('user_report_create');
            Route::post('user_day_report','reportsCtrl@user_day_report')->name('user_day_report');
            Route::post('client_report','reportsCtrl@client_report')->name('client_report');
        });


    });

    Route::post('filter','HomeController@filter')->name('filter')->middleware(['role:admin']);
    Route::get('reset_password',function (){
        $user = auth()->user();
        return view('auth.passwords.reset',compact('user'));
    })->name('reset_password');


    Route::namespace('backend')->middleware(['role:user'])->prefix('user')->group(function () {
        Route::post('start_new_line','itinerariesCtrl@start')->name('start_new_line');
        Route::post('end_line/{id}','itinerariesCtrl@end')->name('end_line');
        Route::get('create_order/{slug}','orderCtrl@create')->name('create_order');
        Route::post('store_order/{slug}','orderCtrl@store')->name('store_order');
        Route::get('user_itineraries_all','itinerariesCtrl@allOfUser')->name('user_itineraries_all');
        Route::get('user_products_all','productsCtrl@user_products_all')->name('user_products_all');
        Route::get('user_itineraries_show/{slug}','itinerariesCtrl@itemOfUser')->name('user_itineraries_show');
    });
});

