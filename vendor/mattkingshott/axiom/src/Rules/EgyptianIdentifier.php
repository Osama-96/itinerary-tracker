<?php declare(strict_types = 1);

namespace Axiom\Rules;

use Axiom\Types\Rule;

class TelephoneNumber extends Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * The telephone number must be 7 - 15 characters in length,
     * and comprised entirely of integers.
     *
     **/
    public function passes($attribute, $value) : bool
    {
        return preg_match('/(2|3)[0-9][1-9][0-1][1-9][0-3][1-9](01|02|03|04|11|12|13|14|15|16|17|18|19|21|22|23|24|25|26|27|28|29|31|32|33|34|35|88)\d\d\d\d\d/', $value) > 0;
    }



    /**
     * Get the validation error message.
     *
     **/
    public function message() : string
    {
        return $this->getLocalizedErrorMessage(
            'telephone_number',
            ' أدخل رقم بطاقة صحيح مكون من 14 رقم يبدأ بـ (2) أو بـ (3) .'
        );
    }
}
