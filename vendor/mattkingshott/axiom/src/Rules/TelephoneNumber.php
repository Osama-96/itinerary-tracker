<?php declare(strict_types = 1);

namespace Axiom\Rules;

use Axiom\Types\Rule;

class TelephoneNumber extends Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * The telephone number must be 7 - 15 characters in length,
     * and comprised entirely of integers.
     *
     **/
    public function passes($attribute, $value) : bool
    {
        return preg_match('/^01[0-9]{9}\z/', $value) > 0;
    }



    /**
     * Get the validation error message.
     *
     **/
    public function message() : string
    {
        return $this->getLocalizedErrorMessage(
            'telephone_number',
            ' أدخل رقم هاتف صحيح مكون من 11 رقم يبدأ بـ (01)..'
        );
    }
}
