@extends('backend.app')
@section('title') المندوبين | {{$item->name}} @stop
@push('cssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/select2.min.css')}}">
@endpush
@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">العملاء</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin_users_all')}}">المندوبين</a></li>
                        <li class="breadcrumb-item active">{{$item->name}}</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->
        <div class="row justify-content-center">
            <div class="col-md-12 table-responsive">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        {{$item->name}}
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr><th>الاسم</th> <td>{{$item->name}}</td></tr>
                            <tr><th>الإيميل</th> <td>{{$item->email}}</td></tr>
                            <tr><th>الرقم القومي</th> <td>{{$item->personal_id}}</td></tr>
                            <tr><th>الهاتف</th> <td>{{$item->phone}}</td></tr>
                            <tr><th>العنوان </th> <td>{{$item->address}}</td></tr>
                            <tr><th>تاريخ بدء الوظيفة </th> <td>{{$item->startJobDate}}</td></tr>

                            <tr><th>المحافظة</th>
                                <td>
                                    @if($item->governorates)
                                        @foreach($item->governorates as $val)
                                            <span class="border px-5 py-2 mx-1">({{$loop->iteration}}) {{getGovernorateName($val)}}</span>
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr><th>عدد خطوط السير التي أتمها</th>
                                <td>
                                   {{$item->itineraries()->count()}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer bg-white text-center ">
                        <div class="row justify-content-center my-3">
                            <div class="col-md-2">
                                <a  class="btn btn-primary form-control text-white" data-toggle="modal"
                                    data-target="#edit_client_{{$item->id}}"><i
                                        class="fa fa-pencil m-r-5"></i> تعديل</a>
                                <!-- Edit Client Modal -->
                                <div id="edit_client_{{$item->id}}" class="modal custom-modal fade" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">تعديل بيانات المندوب: {{$item->name}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{route('admin_users_update',$item)}}"  method="Post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label"> اسم المندوب <span
                                                                        class="text-danger">*</span></label>
                                                                <input class="form-control  @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name',$item->name)}}">
                                                                @error('name')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">الإيميل </label>
                                                                <input class="form-control  @error('email') is-invalid @enderror" type="email" name="email" value="{{old('email',$item->email)}}">
                                                                @error('email')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label"> رقم القومي <span
                                                                        class="text-danger">*</span></label>
                                                                <input class="form-control @error('personal_id') is-invalid @enderror " type="text" name="personal_id" value="{{old('personal_id',$item->personal_id)}}">
                                                                @error('personal_id')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">رقم الهاتف </label>
                                                                <input class="form-control  @error('phone') is-invalid @enderror" type="text" name="phone" value="{{old('phone',$item->phone)}}">
                                                                @error('phone')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">العنوان </label>
                                                                <input class="form-control  @error('address') is-invalid @enderror" type="text" name="address" value="{{old('address',$item->address)}}">
                                                                @error('address')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">تاريخ بدء الوظيفة</label>
                                                                <input class="form-control  @error('startJobDate') is-invalid @enderror" type="date" name="startJobDate" value="{{old('startJobDate',($item->startJobDate)?$item->startJobDate->format('Y-m-d'):'')}}">
                                                                @error('startJobDate')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label">المحافظات المسؤول عنها</label>
                                                                <div class="form-group form-focus select-focus focused">
                                                                    <select name="governorates[]" class="select floating select2-hidden-accessible" data-select2-id="1"
                                                                            tabindex="-1" aria-hidden="true" multiple>
                                                                        <option data-select2-id="3">اختر المحافظات</option>
                                                                        @foreach(DB::table('governorates')->select('name','id')->get() as $key=>$gov)
                                                                            <option value="{{$gov->id}}" @if(old('governorates') && in_array($gov->id,old('governorates')))selected @elseif($item->governorates && in_array($gov->id, $item->governorates )) selected @endif>{{$gov->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <label class="focus-label">المحافظات المسؤول عنها</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="submit-section">
                                                        <input type="submit" class="btn btn-primary submit-btn" value="تحديث البيانات">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /Edit Client Modal -->
                    </div>

                </div>
            </div>
        </div>
    </div>


@stop

@push('footerScripts')
    <script src="{{asset('backend/assets/js/select2.min.js')}}"></script>
@endpush

