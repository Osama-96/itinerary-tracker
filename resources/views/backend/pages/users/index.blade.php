@extends('backend.app')
@section('title') جميع المندوبين @stop
@push('cssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/select2.min.css')}}">
@endpush


@section('content')

    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">المندوبين</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item active">المندوبين</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_client"><i
                            class="fa fa-plus"></i> اضافة مندوب</a>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->
        <div class="row filter-row">

            <div class="col-sm-6 col-md-6">
                <div class="form-group form-focus">
                    <form action="" id="filter">
                        @csrf
                    <input type="text" name="name" class="form-control form-control-sm floating" @if(isset($_GET['name'])) value="{{$_GET['name'] }}"@endif>
                    <label class="focus-label">اسم المندوب</label>
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-md-2">
                <a href="#" onclick="$('#filter').submit()" class="btn btn-success btn-sm btn-block"> بحث </a>
            </div>
            @if(isset($_GET['name']))

            <div class="col-sm-3 col-md-2">
                <a href="{{route('admin_users_all')}}"  class="btn btn-primary btn-sm btn-block"> عرض الكل </a>

            </div>
            @endif

        </div>
        <!-- Search Filter -->

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <table
                                    class="table table-striped custom-table datatable dataTable no-footer"
                                    id="DataTables_Table_0" role="grid"
                                    aria-describedby="DataTables_Table_0_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-sort="ascending"
                                            aria-label="Name: activate to sort column descending"
                                            style="width: 201px;">الاسم</th>

                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Mobile: activate to sort column ascending"
                                            style="width: 92px;">الهاتف</th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Mobile: activate to sort column ascending"
                                            style="width: 92px;">العنوان</th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Status: activate to sort column ascending"
                                            style="width: 212px;">المحافظات</th>
                                        <th class="text-right sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Action: activate to sort column ascending"
                                            style="width: 83px;">العمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($data))
                                        @foreach($data as $key=>$item)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">
                                            <h2 class="table-avatar">
                                                <a href="{{route('admin_users_show', $item->slug)}}">{{$item->name}}</a>
                                            </h2>
                                        </td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->address}}</td>

                                        <td>
                                        @if($item->governorates)

                                            @foreach($item->governorates as $val)
                                            <span class="border px-1 py-1 mx-1 mt-1">({{$loop->iteration}}) {{getGovernorateName($val)}}</span>
                                            @endforeach
                                            @endif

                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown dropdown-action">
                                                <a href="#" class="action-icon dropdown-toggle"
                                                   data-toggle="dropdown" aria-expanded="false"><i
                                                        class="fa fa-ellipsis-v"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{route('admin_users_show',$item)}}"><i
                                                            class="fa fa-eye m-r-5"></i> عرض وتعديل</a>
                                                    <a class="dropdown-item" href="#"
                                                       data-toggle="modal"
                                                       data-target="#delete_client_{{$item->id}}"><i
                                                            class="fa fa-trash-o m-r-5"></i> حذف</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="text-muted text-center"> لا يوجد بيانات .. </td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>
                                @foreach($data as $key=>$item)

                                        <!-- Delete Client Modal -->
                                        <div class="modal custom-modal fade" id="delete_client_{{$item->id}}" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="form-header">
                                                            <h3>حذف مندوب: {{$item->name}}</h3>
                                                            <p>هل أنت متأكد من حذف هذا المندوب؟</p>
                                                        </div>
                                                        <form action="{{route('admin_users_destroy',$item->slug)}}"  method="post" id="delete_{{$item->id}}">@csrf</form>
                                                        <div class="modal-btn delete-action">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <a href="javascript:void(0);" onclick="$('#delete_{{$item->id}}').submit()"
                                                                       class="btn btn-primary continue-btn" >حذف</a>
                                                                </div>
                                                                <div class="col-6">
                                                                    <a href="javascript:void(0);" data-dismiss="modal"
                                                                       class="btn btn-primary cancel-btn">تراجع</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Delete Client Modal -->
                                    @endforeach
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="DataTables_Table_0_paginate">
                                    {{$data->onEachSide(1)->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /Page Content -->

    <!-- Add Client Modal -->
    <div id="add_client" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('admin_users_store')}}"  method="Post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label"> اسم المندوب <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control  @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name')}}">
                                    @error('name')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">الإيميل </label>
                                    <input class="form-control  @error('email') is-invalid @enderror" type="email" name="email" value="{{old('email')}}">
                                    @error('email')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">الباسورد </label>
                                    <input class="form-control  @error('password') is-invalid @enderror" type="password" name="password" required>
                                    @error('password')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">تأكيد كلمة المرور </label>
                                    <input class="form-control  @error('password_confirmation') is-invalid @enderror" type="password" name="password_confirmation" required>
                                    @error('password_confirmation')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label"> رقم القومي <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control @error('personal_id') is-invalid @enderror " type="text" name="personal_id" value="{{old('personal_id')}}">
                                    @error('personal_id')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">رقم الهاتف </label>
                                    <input class="form-control  @error('phone') is-invalid @enderror" type="text" name="phone" value="{{old('phone')}}">
                                    @error('phone')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">العنوان </label>
                                    <input class="form-control  @error('address') is-invalid @enderror" type="text" name="address" value="{{old('address')}}">
                                    @error('address')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">تاريخ بدء الوظيفة</label>
                                    <input class="form-control  @error('startJobDate') is-invalid @enderror" type="date" name="startJobDate" value="{{old('startJobDate')}}">
                                    @error('startJobDate')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">المحافظات المسؤول عنها</label>
                                    <div class="form-group form-focus select-focus focused">
                                        <select name="governorates[]" class="select floating select2-hidden-accessible" data-select2-id="2"
                                                tabindex="-1" aria-hidden="true" multiple>
                                            <option data-select2-id="3">اختر المحافظات</option>
                                            @foreach(DB::table('governorates')->select('name','id')->get() as $key=>$gov)
                                                <option value="{{$gov->id}}" @if(old('governorates') && in_array($gov->id,old('governorates')))selected @endif>{{$gov->name}}</option>
                                            @endforeach
                                        </select>
                                        <label class="focus-label">المحافظات المسؤول عنها</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="submit-section">
                            <input type="submit" class="btn btn-primary submit-btn" value="إضافة">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add Client Modal -->

@stop
@push('footerScripts')
    <script src="{{asset('backend/assets/js/select2.min.js')}}"></script>
@endpush
