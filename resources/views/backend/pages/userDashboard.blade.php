@extends('backend.app')
@section('title') لوحة التحكم @stop
@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-7">
                    <h3 class="page-title">خط السير</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item active">
                            @if(!auth()->user()->itineraries()->current()->count())
                                خط سير جديد
                            @else
                                خط السير الحالي
                            @endif
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

                <div class="row">
                    @if(auth()->user()->itineraries()->current()->count())
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <span class="text-primary" style="font-size: 20px">      ملخص عمليات هذا الخط</span>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive my-2">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <th> وقت بداية هذا الخط:</th>
                                                <td>{{auth()->user()->itineraries()->current()->first()->created_at->format('Y-m-d H:m')}}</td>
                                            </tr>
                                            <tr>
                                                <th> عدد العمليات التي تمت في هذا الخط:</th>
                                                <td>{{auth()->user()->itineraries()->current()->first()->orders()->count()}}</td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <a href="{{route('create_order',auth()->user()->itineraries()->current()->first())}}"  class="btn add-btn mx-2"  >
                                        <!-- <i class="fa fa-plus"></i> -->
                                        تسجيل طلب جديد</a>

                                    <form action="{{route('end_line',auth()->user()->itineraries()->current()->first())}}" method="POST" hidden id="eventForm">
                                        @csrf
                                        <input type="text" name="lat" id="lat" hidden >
                                        <input type="text" name="lng" id="lng" hidden >
                                    </form>
                                </div>
                                <div class="card-footer">
                                    <button  class="btn btn-dark mx-2" id="event" >إنهاء خط السير الحالي </button>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <p>قم ببدء خط سير جديد قبل أن تخرج من مكانك ، ثم قم بتسجيل الطلبات في موقع العميل قبل أن تغادر..</p>
                                    <button  class="btn add-btn" id="event" >بدء خط سير</button>
                                    <form action="{{route('start_new_line')}}" method="POST" hidden id="eventForm">
                                        @csrf
                                        <input type="text" name="lat" id="lat" hidden >
                                        <input type="text" name="lng" id="lng" hidden >
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header text-center text-primary" style="font-size: 55px">
                                <i class="fa fa-truck"></i>
                            </div>
                            <div class="card-body p-1 text-center">
                                <h2>{{auth()->user()->itineraries()->ended()->count()}}</h2>
                                <h4>خطوطي السابقة</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header text-center text-primary" style="font-size: 55px">
                                <i class="fa fa-calendar-check-o"></i>
                            </div>
                            <div class="card-body p-1 text-center">
                                <h2>{{auth()->user()->orders()->count()}}</h2>
                                <h4> الطلبات التي سجلتها </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        @stop

        @section('js')

            <script>


                $("#event").click(function(event){
                    event.preventDefault();
                    if (navigator.geolocation) { // Supported
                        // To add PositionOptions
                        return  navigator.geolocation.getCurrentPosition(function(position){
                            $('#lat').val( position.coords.latitude);
                            $('#lng').val( position.coords.longitude);
                            $(this).prop("disabled", true);
                            $('#eventForm').submit();
                        });
                    } else { // Not supported
                        alert("من فضلك قم باستخدام متصفح آخر يسمح بالتفاط الموقع");
                    }

                });


            </script>
@stop
