@extends('backend.app')
@section('title') المنتجات | {{$item->name}} @stop
@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">المنتجات</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin_products_all')}}">المنتجات</a></li>
                        <li class="breadcrumb-item active">{{$item->name}}</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->

        <!-- /Page Header -->
        <div class="row justify-content-center">
            <div class="col-md-12 table-responsive">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        {{$item->name}}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8 table-responsive">
                                <table class="table">
                                    <tr><th>الاسم</th> <td>{{$item->name}}</td></tr>
                                    <tr><th>الصنف</th><td><a href="{{route('admin_categories_show',$item->category->slug)}}" class="btn btn-link">{{$item->category->name}} </a> </td>
                                    </tr>
                                    <tr><th>السعر</th> <td>{{$item->cost}} ج.م</td></tr>
                                </table>
                            </div>
                            <div class="col-md-4">
                                <a href="#" class="btn btn-link"
                                   data-toggle="modal"
                                   data-target="#image_client_{{$item->id}}">
                                    <img src="{{asset($item->image)}}" alt="" class="img-fluid">
                                </a>
                                <!-- Image Client Modal -->
                                <div class="modal custom-modal fade bd-example-modal-xl" id="image_client_{{$item->id}}" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered  modal-xl">

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">{{$item->name}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="@if($item->image){{asset($item->image)}}@endif" id="imgPreview-{{$item->id}}" class="card-img" alt="{{$item->image}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Image Client Modal -->
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-white text-center ">
                        <div class="row justify-content-center my-3">
                            <div class="col-md-2">
                                <a  class="btn btn-primary form-control text-white" data-toggle="modal"
                                    data-target="#edit_product_{{$item->id}}"><i
                                        class="fa fa-pencil m-r-5"></i> تعديل</a>
                                <!-- Edit Client Modal -->
                                <!-- Edit Client Modal -->
                                <div id="edit_product_{{$item->id}}" class="modal custom-modal fade" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">تعديل بيانات منتج: {{$item->name}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{route('admin_products_update',$item->slug)}}" enctype="multipart/form-data" method="Post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label"> اسم المنتج <span
                                                                        class="text-danger">*</span></label>
                                                                <input class="form-control  @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name',$item->name)}}">
                                                                @error('name')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label"> الصنف  <span
                                                                        class="text-danger">*</span></label>

                                                                <select name="category_id" id="" class="form-control  @error('category_id') is-invalid @enderror">
                                                                    <option value="">اختر الصنف</option>
                                                                    @foreach(\DB::table('categories')->select('id','name')->get() as $key => $cat)
                                                                        <option value="{{$cat->id}}" @if (old('category_id',$item->category_id) == $cat->id) selected @endif>{{$cat->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('category_id')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">سعر المنتج  </label>
                                                                <input class="form-control  @error('cost') is-invalid @enderror" type="number" min="0" step="0.01"  name="cost" value="{{old('cost',$item->cost)}}">
                                                                @error('cost')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="col-form-label"> الصورة </label>
                                                            <input class="form-control-file  @error('image') is-invalid @enderror"  onchange="readURL(this,'#imgPreview')" type="file" name="image">
                                                            @error('image')
                                                            <small class="text-danger">{{$message}}</small>
                                                            @enderror

                                                        </div>

                                                        <div class="col-md-12">
                                                            <img src="@if($item->image){{asset($item->image)}}@endif" id="imgPreview" class="card-img" alt="">
                                                        </div>


                                                    </div>
                                                    <div class="submit-section">
                                                        <input type="submit" class="btn btn-primary submit-btn" value="تحديث البيانات">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Edit Client Modal -->
                            </div>
                        </div>
                        <!-- /Edit Client Modal -->
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop
