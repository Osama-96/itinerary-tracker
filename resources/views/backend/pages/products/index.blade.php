@extends('backend.app')
@section('title') جميع المنتجات @stop
@push('cssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/dataTables.bootstrap.min.css')}}">
@endpush


@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">العملاء</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item active">المنتجات</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_client"><i
                            class="fa fa-plus"></i> اضافة منتج</a>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->
        <div class="row filter-row">

            <div class="col-sm-6 col-md-6">
                <div class="form-group form-focus">
                    <form id="filter">
                        @csrf
                        <input type="text" name="name" class="form-control form-control-sm floating" @if(isset($_GET['name'])) value="{{$_GET['name'] }}"@endif>
                        <label class="focus-label">اسم المنتج</label>
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-md-2">
                <a href="#" onclick="$('#filter').submit()" class="btn btn-success btn-sm btn-block"> بحث </a>
            </div>
            @if(isset($_GET['name']))
                <div class="col-sm-3 col-md-2">
                    <a href="{{route('admin_products_all')}}"  class="btn btn-primary btn-sm btn-block"> عرض الكل </a>
                </div>
            @endif
        </div>
        <!-- Search Filter -->


        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <table
                                    class="table table-striped custom-table datatable dataTable no-footer"
                                    id="DataTables_Table_0" role="grid"
                                    aria-describedby="DataTables_Table_0_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-sort="ascending"
                                            aria-label="Name: activate to sort column descending"
                                            style="width: 201px;">الاسم</th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Client ID: activate to sort column ascending"
                                            style="width: 104px;">الصورة</th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Mobile: activate to sort column ascending"
                                            style="width: 92px;">السعر</th>
                                        <th class="text-right sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Action: activate to sort column ascending"
                                            style="width: 83px;">العمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data->count() > 0)
                                        @foreach($data as  $key=>$item)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">
                                                    <h2 class="table-avatar">
                                                        <a href="{{route('admin_products_show', $item->slug)}}">{{$item->name}}</a>
                                                    </h2>
                                                </td>
                                                <td>
                                                    @if($item->image)
                                                        <a href="#" class="btn btn-link"
                                                           data-toggle="modal"
                                                           data-target="#image_client_{{$item->id}}"><i class="fa fa-image"> </i> عرض</a>
                                                    @else
                                                        <span class="text-muted">غير متوفر</span>
                                                    @endif
                                                </td>
                                                <td>{{$item->cost??''}} ج.م</td>
                                                <td class="text-right">
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle"
                                                           data-toggle="dropdown" aria-expanded="false"><i
                                                                class="fa fa-ellipsis-v"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{route('admin_products_show',$item)}}"><i
                                                                    class="fa fa-eye m-r-5"></i> تعديل وعرض</a>
                                                            <a class="dropdown-item" href="#"
                                                               data-toggle="modal"
                                                               data-target="#delete_client_{{$item->id}}"><i
                                                                    class="fa fa-trash-o m-r-5"></i> حذف</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="text-muted text-center"> لا يوجد بيانات .. </td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>

                            @if(count($data) > 0)
                                @foreach($data as $key=>$item)



                                    <!-- Delete Client Modal -->
                                        <div class="modal custom-modal fade " id="delete_client_{{$item->id}}" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="form-header">
                                                            <h3>حذف منتج: {{$item->name}}</h3>
                                                            <p>هل أنت متأكد من حذف هذا المنتج؟</p>
                                                        </div>
                                                        <form action="{{route('admin_products_destroy',$item->slug)}}" method="post" id="delete_{{$item->id}}">@csrf</form>
                                                        <div class="modal-btn delete-action">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <a href="javascript:void(0);" onclick="$('#delete_{{$item->id}}').submit()"
                                                                       class="btn btn-primary continue-btn" >حذف</a>
                                                                </div>
                                                                <div class="col-6">
                                                                    <a href="javascript:void(0);" data-dismiss="modal"
                                                                       class="btn btn-primary cancel-btn">تراجع</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Delete Client Modal -->
                                        <!-- Image Client Modal -->
                                        <div class="modal custom-modal fade bd-example-modal-xl" id="image_client_{{$item->id}}" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered  modal-xl">

                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">{{$item->name}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="@if($item->image){{asset($item->image)}}@endif" id="imgPreview-{{$item->id}}" class="card-img" alt="{{$item->image}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Image Client Modal -->
                                    @endforeach
                                @endif
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="DataTables_Table_0_paginate">
                                    {{$data->onEachSide(1)->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /Page Content -->

    <!-- Add Client Modal -->
    <div id="add_client" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('admin_products_store')}}" enctype="multipart/form-data" method="Post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label"> اسم المنتج <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control  @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name')}}">
                                    @error('name')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label"> الصنف  <span
                                            class="text-danger">*</span></label>
                                    <select name="category_id" id="" class="form-control  @error('category_id') is-invalid @enderror">
                                        <option value="">اختر الصنف</option>
                                        @foreach(\DB::table('categories')->select('id','name')->get() as $key => $cat)
                                            <option value="{{$cat->id}}" @if (old('category_id') == $cat->id) selected @endif>{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">سعر المنتج  </label>
                                    <input class="form-control  @error('cost') is-invalid @enderror" type="number" name="cost" step="0.01" value="{{old('cost')}}">
                                    @error('cost')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label"> الصورة </label>
                                <input class="form-control-file  @error('image') is-invalid @enderror" onchange="readURL(this,'#imgprev')" type="file" name="image">
                                @error('image')
                                <small class="text-danger">{{$message}}</small>
                                @enderror

                            </div>
                            <div class="col-md-12">
                                <img src="" alt="" id="imgprev" class="img-fluid">
                            </div>

                        </div>
                        <div class="submit-section">
                            <input type="submit" class="btn btn-primary submit-btn" value="إضافة">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add Client Modal -->

@stop
@push('footerScripts')
    <script src="{{asset('backend/assets/js/select2.min.js')}}"></script>
@endpush

