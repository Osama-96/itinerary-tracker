@extends('backend.app')
@section('title') لوحة التحكم @stop
@section('content')
<!-- Page Content -->
<div class="content container-fluid">
    <!-- Page Header -->
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="page-title">نتيجة البحث</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item ">
                        <a href="{{url('/')}}" >
                        لوحة التحكم
                        </a>
                    </li>
                    <li class="breadcrumb-item active">نتيجة البحث </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /Page Header -->

    <div class="row">

        <div class="col-md-4 col-sm-12 col-lg-4 col-xl-4">
            <div class="card dash-widget">
                <div class="card-header bg-dark text-white text-center">
                    <span style="font-size: 20px;">المندوبين</span>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        @if(count($users))
                            @foreach($users as $key => $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td><a href="{{route('admin_users_show',$user->slug)}}">عرض</a> </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2"> لا توجد نتائج مطابقة </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-lg-4 col-xl-4">
            <div class="card dash-widget">
                <div class="card-header bg-dark text-white text-center">
                    <span style="font-size: 20px;">العملاء</span>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        @if(count($clients))
                            @foreach($clients as $key => $client)
                                <tr>
                                    <td>{{$client->name}}</td>
                                    <td><a href="{{route('admin_clients_show',$client->slug)}}">عرض</a> </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2"> لا توجد نتائج مطابقة </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-lg-4 col-xl-4">
            <div class="card dash-widget">
                <div class="card-header bg-dark text-white text-center">
                    <span style="font-size: 20px;">المنتجات</span>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        @if(count($products))
                            @foreach($products as $key => $product)
                                <tr>
                                    <td>{{$product->name}}</td>
                                    <td><a href="{{route('admin_products_show',$product->slug)}}">عرض</a> </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2"> لا توجد نتائج مطابقة </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- /Page Content -->
@stop
