@extends('backend.app')
@section('title') جميع الأصناف @stop
@push('cssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/dataTables.bootstrap.min.css')}}">
@endpush


@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">الأصناف</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item active">الأصناف</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_client"><i
                            class="fa fa-plus"></i> اضافة صنف</a>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->
        <div class="row filter-row">

            <div class="col-sm-6 col-md-6">
                <div class="form-group form-focus">
                    <form id="filter">
                        @csrf
                        <input type="text" name="name" class="form-control form-control-sm floating" @if(isset($_GET['name'])) value="{{$_GET['name'] }}"@endif>
                        <label class="focus-label">اسم الصنف</label>
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-md-2">
                <a href="#" onclick="$('#filter').submit()" class="btn btn-success btn-sm btn-block"> بحث </a>
            </div>
            @if(isset($_GET['name']))
                <div class="col-sm-3 col-md-2">
                    <a href="{{route('admin_products_all')}}"  class="btn btn-primary btn-sm btn-block"> عرض الكل </a>
                </div>
            @endif
        </div>
        <!-- Search Filter -->


        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <table
                                    class="table table-striped custom-table datatable dataTable no-footer"
                                    id="DataTables_Table_0" role="grid"
                                    aria-describedby="DataTables_Table_0_info">
                                    <thead>
                                    <tr role="row">
                                        <th style="width: 30px;"> # </th>
                                        <th class="sorting_asc" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-sort="ascending"
                                            aria-label="Name: activate to sort column descending"
                                            style="width: 201px;">الاسم</th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Client ID: activate to sort column ascending"
                                            style="width: 104px;">عدد المنتجات</th>
                                        <th class="text-right sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Action: activate to sort column ascending"
                                            style="width: 83px;">العمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data->count() > 0)
                                        @foreach($data as  $key=>$item)
                                            <tr role="row" class="odd">
                                                <td >{{$key+1}}</td>
                                                <td>
                                                    <h2 class="table-avatar">
                                                        <a href="{{route('admin_categories_show', $item->slug)}}">{{$item->name}}</a>
                                                    </h2>
                                                </td>

                                                <td>{{$item->products()->count()}} </td>
                                                <td class="text-right">
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle"
                                                           data-toggle="dropdown" aria-expanded="false"><i
                                                                class="fa fa-ellipsis-v"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{route('admin_categories_show',$item)}}"><i
                                                                    class="fa fa-eye m-r-5"></i>عرض وتعديل </a>
                                                            <a class="dropdown-item" href="#"
                                                               data-toggle="modal"
                                                               data-target="#delete_client_{{$item->id}}"><i
                                                                    class="fa fa-trash-o m-r-5"></i> حذف</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="text-muted text-center"> لا يوجد بيانات .. </td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>

                            @if(count($data) > 0)
                            @foreach($data as $key=>$item)

                                    <!-- Delete Client Modal -->
                                    <div class="modal custom-modal fade " id="delete_client_{{$item->id}}" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="form-header">
                                                        <h3>حذف الصنف: {{$item->name}}</h3>
                                                        <p>هل أنت متأكد من حذف هذا الصنف؟</p>
                                                    </div>
                                                    <form action="{{route('admin_categories_destroy',$item->slug)}}" method="post" id="delete_{{$item->id}}">@csrf</form>
                                                    <div class="modal-btn delete-action">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <a href="javascript:void(0);" onclick="$('#delete_{{$item->id}}').submit()"
                                                                   class="btn btn-primary continue-btn" >حذف</a>
                                                            </div>
                                                            <div class="col-6">
                                                                <a href="javascript:void(0);" data-dismiss="modal"
                                                                   class="btn btn-primary cancel-btn">تراجع</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Delete Client Modal -->

                                @endforeach
                            @endif
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="DataTables_Table_0_paginate">
                                    {{$data->onEachSide(1)->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /Page Content -->

    <!-- Add Client Modal -->
    <div id="add_client" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('admin_categories_store')}}" enctype="multipart/form-data" method="Post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label"> اسم الصنف <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control  @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name')}}" required>
                                    @error('name')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="submit-section">
                            <input type="submit" class="btn btn-primary submit-btn" value="إضافة">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add Client Modal -->

@stop
@push('footerScripts')
    <script src="{{asset('backend/assets/js/select2.min.js')}}"></script>
@endpush

