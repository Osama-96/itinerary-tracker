@extends('backend.app')
@section('title') الأصناف | {{$item->name}} @stop
@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">الأصناف</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin_categories_all')}}">الأصناف</a></li>
                        <li class="breadcrumb-item active">{{$item->name}}</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->

        <!-- /Page Header -->
        <div class="row justify-content-center">
            <div class="col-md-12 table-responsive">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        {{$item->name}}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8 table-responsive">
                                <table class="table">
                                    <tr><th>الاسم</th> <td>{{$item->name}}</td></tr>
                                    <tr><th>عدد المنتجات</th> <td>{{$item->products()->count()}} </td></tr>
                                </table>
                            </div>
                            <div class="col-md-4 table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr class="bg-dark text-white">
                                            <th> المنتجات تحت هذا الصنف </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if($item->products()->count())
                                        @foreach($item->products as  $key=>$pro)
                                            <tr>
                                                <td><a href="{{route('admin_products_show',$pro->slug)}}" class="btn btn-link">{{$pro->name}} </a> </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <td>
                                            لا يوجد منتجات
                                        </td>
                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-white text-center ">
                        <div class="row justify-content-center my-3">
                            <div class="col-md-2">
                                <a  class="btn btn-primary form-control text-white" data-toggle="modal"
                                    data-target="#edit_product_{{$item->id}}"><i
                                        class="fa fa-pencil m-r-5"></i> تعديل</a>
                                <!-- Edit Client Modal -->
                                <!-- Edit Client Modal -->
                                <div id="edit_product_{{$item->id}}" class="modal custom-modal fade" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">تعديل بيانات صنف: {{$item->name}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{route('admin_categories_update',$item->slug)}}" enctype="multipart/form-data" method="Post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label"> اسم الصنف <span
                                                                        class="text-danger">*</span></label>
                                                                <input class="form-control  @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name',$item->name)}}">
                                                                @error('name')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <img src="@if($item->image){{asset($item->image)}}@endif" id="imgPreview" class="card-img" alt="">
                                                        </div>


                                                    </div>
                                                    <div class="submit-section">
                                                        <input type="submit" class="btn btn-primary submit-btn" value="تحديث البيانات">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Edit Client Modal -->
                            </div>
                        </div>
                        <!-- /Edit Client Modal -->
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop
