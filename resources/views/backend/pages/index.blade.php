@extends('backend.app')
@section('title') لوحة التحكم @stop
@section('content')
<!-- Page Content -->
<div class="content container-fluid">
    <!-- Page Header -->
    <div class="page-header">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="page-title">مرحباً {{auth()->user()->name}}</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item active">لوحة التحكم</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /Page Header -->

    <div class="row">

        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
            <div class="card dash-widget">
                <div class="card-body">
                    <span class="dash-widget-icon"><i class="fa fa-users"></i></span>
                    <div class="dash-widget-info">
                        <h3>{{App\Client::count()}}</h3>
                        <span>عملاء</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
            <div class="card dash-widget">
                <div class="card-body">
                    <span class="dash-widget-icon"><i class="fa fa-user"></i></span>
                    <div class="dash-widget-info">
                        <h3>{{App\User::role('user')->count()}}</h3>
                        <span>مندوبين</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
            <div class="card dash-widget">
                <div class="card-body">
                    <span class="dash-widget-icon"><i class="fa fa-list"></i></span>
                    <div class="dash-widget-info">
                        <h3>{{App\Category::count()}}</h3>
                        <span>أصناف</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
            <div class="card dash-widget">
                <div class="card-body">
                    <span class="dash-widget-icon"><i class="fa fa-cubes"></i></span>
                    <div class="dash-widget-info">
                        <h3>{{App\Product::count()}}</h3>
                        <span>منتجات</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
            <div class="card dash-widget">
                <div class="card-body">
                    <span class="dash-widget-icon"><i class="fa fa-truck"></i></span>
                    <div class="dash-widget-info">
                        <h3>{{App\Itinerary::count()}}</h3>
                        <span>خطوط السير الحالية</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
            <div class="card dash-widget">
                <div class="card-body">
                    <span class="dash-widget-icon"><i class="fa fa-calendar-check-o"></i></span>
                    <div class="dash-widget-info">
                        <h3>{{App\Itinerary::count()}}</h3>
                        <span>خطوط السير المنتهية</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
            <div class="card dash-widget">
                <div class="card-body">
                    <span class="dash-widget-icon"><i class="fa fa-cart-arrow-down"></i></span>
                    <div class="dash-widget-info">
                        <h3>{{App\Order::count()}}</h3>
                        <span> عدد الطلبات المحفوظة</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
            <div class="card dash-widget">
                <div class="card-body">
                    <span class="dash-widget-icon"><i class="fa fa-arrow-circle-left"></i></span>
                    <div class="dash-widget-info">
                        <h3  class="text-muted">للمزيد</h3>
                        <span class="text-muted">تصفح القوائم .. </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-6 text-center">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-body">--}}
{{--                            <h3 class="card-title">Total Revenue</h3>--}}
{{--                            <div id="bar-charts" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-6 text-center">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-body">--}}
{{--                            <h3 class="card-title">Sales Overview</h3>--}}
{{--                            <div id="line-charts" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
<!-- /Page Content -->
@stop
