@extends('backend.app')
@section('title') التقارير  @stop
@push('cssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/select2.min.css')}}">
@endpush


@section('content')

    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">التقارير</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        @if(url()->current() == route('user_day_report'))
                        <li class="breadcrumb-item active">تقرير عن أحد المندوبين في فترة محددة</li>
                        @elseif (url()->current() == route('client_report'))
                        <li class="breadcrumb-item active">تقرير عن أحد العملاء في فترة محددة</li>
                        @endif

                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        @if(url()->current() == route('user_day_report'))
            <div class="row ">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-primary text-center">
                            <span>
                             البيانات الأساسية للتقرير
                            </span>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table">
                                <tr><th>المندوب:</th> <td>{{$user->name}}</td></tr>
                                <tr><th>تاريخ البدء:</th> <td>{{\Carbon\Carbon::create($from)->format('Y/m/d')}}</td></tr>
                                <tr><th>تاريخ الانتهاء:</th> <td>{{\Carbon\Carbon::create($to)->format('Y/m/d')}}</td></tr>
                                <tr><th>عدد خطوط السير في هذه الفترة:</th> <td>{{count($data)?count($data):0}}</td></tr>
                                <tr><th>إجمالي المسافات التي قطعها في هذه الفترة:</th> <td>{{$distance}} كيلو متر </td></tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3">
                    <div class="card">
                        <div class="card-header bg-primary text-center">
                            <span>
                            خطوط السير التي قام بها
                            </span>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table">
                                <th class="sorting" tabindex="0"
                                    aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                    aria-label="Client ID: activate to sort column ascending"
                                    style="min-width: 200px;">وقت البدء</th>
                                <th class="sorting" tabindex="0"
                                    aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                    aria-label="Mobile: activate to sort column ascending"
                                    style="min-width: 200px;">الحالة</th>
                                <th class="sorting" tabindex="0"
                                    aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                    aria-label="Status: activate to sort column ascending"
                                    style="min-width: 150px;">عدد الطلبات</th>
                                <th class="sorting" tabindex="0"
                                    aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                    aria-label="Status: activate to sort column ascending"
                                    style="min-width: 150px;"> المسافة المقطوعة</th>
                                <th class="text-center sorting" tabindex="0"
                                    aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                    aria-label="Action: activate to sort column ascending"
                                    style="min-width: 100px;"></th>
                                </tr>
                                @foreach($data as $key=>$val)
                                <tr>
                                    <td>{{$val->start->format('Y-m-d h:i')}}</td>
                                    <td>{{$val->status}}</td>
                                    <td>{{$val->orders()->count()}}</td>
                                    <td>{{$val->distance? $val->distance.' كيلو متر ' :'لم يتم حفظ المسافة'}}</td>
                                    <td><a href="{{route('admin_itineraries_show',$val->slug)}}" class="btn btn-link"> عرض</a> </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                <div class="dataTables_paginate paging_simple_numbers"
                     id="DataTables_Table_0_paginate">
                    {{$data->onEachSide(1)->links()}}
                </div>
            </div>
            </div>
        @elseif (url()->current() == route('client_report'))
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-primary text-center">
                            <span>
                             البيانات الأساسية للتقرير
                            </span>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table">
                            <tr><th>العميل:</th> <td>{{$client->name}}</td></tr>
                            <tr><th>تاريخ البدء:</th> <td>{{\Carbon\Carbon::create($from)->format('Y/m/d')}}</td></tr>
                            <tr><th>تاريخ الانتهاء:</th> <td>{{\Carbon\Carbon::create($to)->format('Y/m/d')}}</td></tr>
                            <tr><th>عدد الطلبات التي تمت له:</th> <td>{{count($data)?count($data):0}}</td></tr>
                        </table>
                    </div>
                </div>
            </div>
            @foreach($data as $key => $order)
            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header bg-dark text-center text-white">
                        <span style="font-size: 25px">
                            عملية رقم {{$key +1}}
                        </span>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table">
                            <tr class="text-center">
                                <th style="min-width:150px">المندوب</th>
                                <th style="min-width:150px">الوقت</th>
                                <th style="min-width:150px">العميل</th>
                                <th style="min-width:150px">موقع العملية</th>
                                <th style="min-width:150px">موقع العميل</th>
                                <th style="min-width:150px"> حالة الوصول</th>
                            </tr>
                            <tr class="text-center">
                                <td>{{$order->user->name}}</td>
                                <td>{{$order->created_at->format('Y-m-d h:i')}}</td>
                                <td>{{$order->client->name}}</td>
                                <td><a href=" https://maps.google.com/maps?q=loc:{{$order->location['lat'].','.$order->location['lng']}}" class="btn btn-link btn-sm" target="_blank"> عرض</a> </td>
                                <td><a href=" https://maps.google.com/maps?q=loc:{{$order->client->location['lat'].','.$order->client->location['lng']}}" class="btn btn-link btn-sm" target="_blank"> عرض</a> </td>
                                <td> <span  class=" {{getAllowFarIndex(getTomTomDistatnce([0=>$order->location , 1=> $order->client->location ],true))['class']}}"> <p class="m-2">{{getAllowFarIndex(getTomTomDistatnce([0=>$order->location , 1=> $order->client->location ],true))['text']}}</p></span> </td>
                            </tr>
                        </table>
                        <table class="table mt-3">
                            <thead class="bg-primary text-white text-center">
                            <tr>
                                <th colspan="3">
                                    تفاصيل العملية
                                </th>
                            </tr>
                            </thead>
                            <tr>
                                <th  style="min-width:150px">غرض العملية:</th><td colspan="2" >{{$order->text_type}}</td>
                            </tr>
                            @if((integer)$order->data['type'] == 1)
                                <tr class=" text-center" style="background-color: #d0d3d4">
                                    <th colspan="2">اسم المنتج</th>
                                    <th>الكمية</th>
                                </tr>
                                @foreach($order->data['products_ids'] as $key => $pro)
                                    <tr class="text-center">
                                        <td style="min-width:150px" colspan="2">{{getItemById('products',$pro)->name}}</td>
                                        <td>{{$order->data['products_qty'][$key]}}  </td>
                                    </tr>
                                @endforeach

                            @elseif((integer)$order->data['type'] == 2)
                                <tr class=" text-center" style="background-color: #d0d3d4">
                                    <th colspan="2"> البيان</th>
                                    <th>المبلغ</th>
                                </tr>
                                @foreach($order->data['received_notice'] as $key => $notice)
                                    <tr class="text-center">
                                        <td style="min-width:150px" colspan="2">{{$notice}}</td> <td>{{$order->data['received_money'][$key]}} ج.م </td>
                                    </tr>
                                @endforeach

                            @endif
                            <tr class=" text-center" style="background-color: #dde2e2">
                                <th colspan="3">ملاحظات</th>
                            </tr>
                            <tr class="text-center">
                                <td style="min-width:150px" colspan="3" >{{$order->data['notes']??'لا يوجد'}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-sm-12 col-md-7">
                <div class="dataTables_paginate paging_simple_numbers"
                     id="DataTables_Table_0_paginate">
                    {{$data->onEachSide(1)->links()}}
                </div>
            </div>
        </div>
        @endif

    </div>
    <!-- /Page Content -->


@stop
@push('footerScripts')
    <script src="{{asset('backend/assets/js/select2.min.js')}}"></script>
@endpush
