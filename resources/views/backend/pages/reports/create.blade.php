@extends('backend.app')
@section('title') إنشاء التقارير  @stop
@push('cssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/select2.min.css')}}">
@endpush


@section('content')

    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">إنشاء التقارير</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item active">إنشاء التقارير</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->
        <div class="row">
            <div class="col-md-6 col-sm-12 col-lg-6 col-xl-6">
                <div class="card dash-widget">
                    <div class="card-body">
                        <span class="dash-widget-icon"><i class="fa fa-user"></i></span>
                        <div class="dash-widget-info">
                            <span class="text-lg">إنشاء تقرير بالفترة عن أحد المندوبين </span>
                        </div>
                        <br  class="my-3">
                        <div>
                            <form action="{{route('user_day_report')}}" class="mt-3" method="POST">@csrf
                                <div class="form-group">
                                    <lable>اختر المندوب</lable>
                                    <select name="user_id" class="form-control"  required>
                                        <option value="">اختر..</option>
                                        @foreach(\App\User::role('user')->select('id','name')->get() as $key=>$user)
                                            <option value="{{$user->id}}" @if (old('user_id') == $user->id) selected @endif>{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <lable>حدد تاريخ بدء الفترة</lable>
                                    <input type="date" name="from" class="form-control @error('from') is-invalid @enderror" value="{{old('from')}}" required>
                                    @error('from')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <lable>حدد تاريخ انتهاء الفترة</lable>
                                    <input type="date" name="to" class="form-control @error('to') is-invalid @enderror" value="{{old('to')}}" required>
                                    @error('to')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group text-center">
                                    <input type="submit" value="إنشاء تقرير" class="btn btn-outline-dark form-control">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-lg-6 col-xl-6">
                <div class="card dash-widget">
                    <div class="card-body">
                        <span class="dash-widget-icon"><i class="fa fa-users"></i></span>
                        <div class="dash-widget-info">
                            <span class="text-lg">إنشاء تقرير بالفترة عن أحد العملاء </span>
                        </div>

                        <br  class="my-3">
                        <div>
                            <form action="{{route('client_report')}}" class="mt-3" method="POST">@csrf
                                <div class="form-group">
                                    <lable>اختر العميل</lable>
                                    <select name="client_id" class="form-control" required>
                                        <option value="">اختر..</option>
                                        @foreach(\App\Client::select('id','name')->get() as $key=>$user)
                                            <option value="{{$user->id}}" @if(old('client_id') == $user->id) selected @endif>{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <lable>حدد تاريخ بدء الفترة</lable>
                                    <input type="date" name="from2" class="form-control @error('from2') is-invalid @enderror" value="{{old('from2')}}" required>
                                    @error('from2')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <lable>حدد تاريخ انتهاء الفترة</lable>
                                    <input type="date" name="to2" class="form-control @error('to2') is-invalid @enderror" value="{{old('to2')}}" required>
                                    @error('to2')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group text-center">
                                    <input type="submit" value="إنشاء تقرير" class="btn btn-outline-dark form-control ">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /Page Content -->


@stop
@push('footerScripts')
    <script src="{{asset('backend/assets/js/select2.min.js')}}"></script>
@endpush
