@extends('backend.app')
@section('title') العملاء | {{$item->name}} @stop
@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">العملاء</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin_clients_all')}}">العملاء</a></li>
                        <li class="breadcrumb-item active">{{$item->name}}</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->
        <div class="row justify-content-center">
            <div class="col-md-12 table-responsive">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        {{$item->name}}
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr><th>الاسم</th> <td>{{$item->name}}</td></tr>
                            <tr><th>الهاتف</th> <td>{{$item->phone}}</td></tr>
                            <tr><th>المحافظة</th> <td>{{$item->governorateName}}</td></tr>
                            <tr><th>العنوان التفصيلي</th> <td>{{$item->address}}</td></tr>
                            <tr><th>الموقع</th>
                                <td>{{$item->location['lat'] .' , '.$item->location['lng'] }}
                                    <a class="btn btn-link border mx-5" href="https://maps.google.com/maps?q=loc:{{$item->location['lat'] .' , '.$item->location['lng'] }}" target='_blank'><i class="fa fa-map-marker m-r-5"></i>عرض على الخريطة</a>
                                </td></tr>
                        </table>
                    </div>

                    <div class="card-footer bg-white text-center ">
                        <div class="row justify-content-center my-3">
                            <div class="col-md-2">
                                <a  class="btn btn-primary form-control text-white" data-toggle="modal"
                                    data-target="#edit_user_{{$item->id}}"><i
                                        class="fa fa-pencil m-r-5"></i> تعديل</a>
                                <!-- Edit Client Modal -->
                                <div id="edit_user_{{$item->id}}" class="modal custom-modal fade" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">تعديل بيانات العميل: {{$item->name}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{route('admin_clients_update',$item->slug)}}" enctype="multipart/form-data" method="Post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label"> اسم العميل <span
                                                                        class="text-danger">*</span></label>
                                                                <input class="form-control  @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name',$item->name)}}">
                                                                @error('name')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">رقم الهاتف </label>
                                                                <input class="form-control  @error('phone') is-invalid @enderror" type="text" name="phone" value="{{old('phone',$item->phone)}}">
                                                                @error('phone')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="col-form-label"> الموقع (خط الطول وخط العرض) <span
                                                                    class="text-danger">*</span></label>
                                                            <input class="form-control  @error('location') is-invalid @enderror" type="text" name="location" value="{{old('location',$item->location['lat'].','.$item->location['lng'])}}">
                                                            @error('location')
                                                            <small class="text-danger">{{$message}}</small>
                                                            @enderror

                                                            {{--                                <div class="row">--}}
                                                            {{--                                    <div class="col-md-8">--}}
                                                            {{--                                        <div class="form-group">--}}

                                                            {{--                                            <input class="form-control" type="text">--}}
                                                            {{--                                        </div>--}}
                                                            {{--                                    </div>--}}
                                                            {{--                                    <div class="col-md-4">--}}
                                                            {{--                                        <a href="" class="btn btn-primary">فتح الخريطة</a>--}}
                                                            {{--                                    </div>--}}
                                                            {{--                                </div>--}}

                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">المحافظة <span
                                                                        class="text-danger">*</span></label>
                                                                <select  class="form-control floating  @error('location') is-invalid @enderror"  name="governorate_id" >
                                                                    @foreach(\App\Governorate::select('id','name')->get() as  $key=>$value)
                                                                        <option value="{{$value->id}}"  @if(old('governorate_id') && old('governorate_id') == $value->id) selected @elseif($item->governorate_id == $value->id) selected  @endif>{{$value->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('governorate_id')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">عنوان تفصيلي</label>
                                                                <input class="form-control  @error('address') is-invalid @enderror" type="text" name="address" value="{{old('address',$item->address)}}">
                                                                @error('address')
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        {{--                            <div class="col-md-6">--}}
                                                        {{--                                <div class="form-group">--}}
                                                        {{--                                    <label class="col-form-label">Confirm Password</label>--}}
                                                        {{--                                    <input class="form-control" type="password">--}}
                                                        {{--                                </div>--}}
                                                        {{--                            </div>--}}
                                                    </div>
                                                    <div class="submit-section">
                                                        <input type="submit" class="btn btn-primary submit-btn" value="تحديث البيانات">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Edit Client Modal -->
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
