@extends('backend.app')
@section('title') خط سير | {{$item->created_format}} @stop
@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">خطوط السير</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin_itineraries_all')}}">خطوط السير</a></li>
                        <li class="breadcrumb-item active">خط سير للمندوب : {{$item->user->name}} بتاريخ:
                        {{$item->created_at->format('Y/m/d')}}</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->
        <div class="row justify-content-center">
            <div class="col-md-4 table-responsive">
                <div class="card">
                    <div class="card-header bg-primary text-white text-center">
                         <i class="fa fa-clock-o h2"></i>
                        <br>
                        نقطة البداية
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <td>الوقت:</td>
                                <td>{{$item->created_format}}</td>
                            </tr>
                            <tr>
                                <td>الموقع:</td>
                                <td><a href=" https://maps.google.com/maps?q=loc:{{$item->start_location['lat'].','.$item->start_location['lng']}}" class="btn btn-link btn-sm" target="_blank"> عرض</a> </td>
                            </tr>

                            <tr>
                                <td>مجموع المسافات التي قطعها:</td>
                                <td>{{$distance}} كيلو متر </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($item->orders()->count())
            <div class="row justify-content-center">
            @foreach($item->orders as $key=>$order)
                <div class="col-md-8 table-responsive">
                    <div class="card">
                        <div class="card-header bg-primary text-white text-center">
                            <i class="fa fa-truck h2"></i>
                            <br>
                            طلب رقم ({{$key+1}})
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table">
                                <tr class="text-center">
                                    <th style="min-width:150px">الوقت</th>
                                    <th style="min-width:150px">العميل</th>
                                    <th style="min-width:150px">موقع العملية</th>
                                    <th style="min-width:150px">موقع العميل</th>
                                    <th style="min-width:150px"> حالة الوصول</th>
                                </tr>
                                <tr class="text-center">
                                    <td>{{$order->created_format}}</td>
                                    <td>{{$order->client->name}}</td>
                                    <td><a href=" https://maps.google.com/maps?q=loc:{{$order->location['lat'].','.$order->location['lng']}}" class="btn btn-link btn-sm" target="_blank"> عرض</a> </td>
                                    <td><a href=" https://maps.google.com/maps?q=loc:{{$order->client->location['lat'].','.$order->client->location['lng']}}" class="btn btn-link btn-sm" target="_blank"> عرض</a> </td>
                                    <td> <span  class=" {{getAllowFarIndex(getTomTomDistatnce([0=>$order->location , 1=> $order->client->location ],true))['class']}}"> <p class="m-2">{{getAllowFarIndex(getTomTomDistatnce([0=>$order->location , 1=> $order->client->location ],true))['text']}}</p></span> </td>
                                </tr>
                            </table>
                            <table class="table mt-3">
                                <thead class="bg-primary text-white text-center">
                                <tr>
                                    <th colspan="3">
                                        تفاصيل العملية
                                    </th>
                                </tr>
                                </thead>
                                <tr>
                                    <th  style="min-width:150px">غرض العملية:</th><td colspan="2" >{{$order->text_type}}</td>
                                </tr>
                                @if((integer)$order->data['type'] == 1)
                                    <tr class=" text-center" style="background-color: #d0d3d4">
                                        <th>اسم المنتج</th>
                                        <th>الكمية</th>
                                        <th>إجمالي</th>
                                    </tr>
                                    <?php $proSum = 0 ;?>
                                    @foreach($order->data['products_ids'] as $key => $pro)
                                        <tr class="text-center">
                                            <td style="min-width:150px" >{{getItemById('products',$pro)->name}}</td>
                                            <td>{{$order->data['products_qty'][$key]}}  </td>
                                            <td>{{$order->data['products_qty'][$key] * getItemById('products',$pro)->cost}} ج.م </td>
                                        </tr>
                                        <?php  $proSum += $order->data['products_qty'][$key] * \DB::table('products')->where('id',$pro)->first()->cost ; ?>
                                    @endforeach
                                    <tr class="text-center">
                                        <th colspan="2">المجموع</th>
                                        <th>{{$proSum}} ج.م </th>
                                    </tr>
                                @elseif((integer)$order->data['type'] == 2)
                                    <tr class=" text-center" style="background-color: #d0d3d4">
                                        <th colspan="2"> البيان</th>
                                        <th>المبلغ</th>
                                    </tr>
                                    <?php $sum=0;?>
                                    @foreach($order->data['received_notice'] as $key => $notice)
                                        <tr class="text-center">
                                            <td style="min-width:150px" colspan="2">{{$notice}}</td> <td>{{$order->data['received_money'][$key]}} ج.م </td>
                                        </tr>
                                        <?php $sum+=$order->data['received_money'][$key] ?>
                                    @endforeach
                                    <tr class="text-center">
                                        <th colspan="2">المجموع</th>
                                        <th>{{$sum}} ج.م </th>
                                    </tr>

                                @endif
                                <tr class=" text-center" style="background-color: #dde2e2">
                                    <th colspan="3">ملاحظات</th>
                                </tr>
                                <tr class="text-center">
                                    <td style="min-width:150px" colspan="3" >{{$order->data['notes']??'لا يوجد'}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @endif
        <div class="row justify-content-center">
            @if($item->ended)
            <div class="col-md-4 table-responsive">
                <div class="card">
                    <div class="card-header bg-primary text-white text-center">
                         <i class="fa fa-clock-o h2"></i>
                        <br>
                        نقطة النهاية
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <td>الوقت:</td>
                                <td>{{$item->end_format}}</td>
                            </tr>
                            <tr>
                                <td>الموقع:</td>
                                <td><a href=" https://maps.google.com/maps?q=loc:{{$item->end_location['lat'].','.$item->end_location['lng']}}" class="btn btn-link btn-sm" target="_blank"> عرض</a> </td>
                            </tr>
                            <tr>
                                <td>البعد عن نقطة البدء:</td>
                                <td>{{getTomTomDistatnce([0=>$item->start_location,1=>$item->end_location ],true)}} كيلو متر</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            @else
            <div class="col-md-3 table-responsive">
                <div class="card">

                    <div class="card-body">
                        <div class="text-center">
                            الخط مازال مستمراً..
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>


@stop
