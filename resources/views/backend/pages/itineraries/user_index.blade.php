@extends('backend.app')
@section('title') جميع خطوط السير @stop
@push('cssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/dataTables.bootstrap.min.css')}}">
@endpush

@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">خطوط السير</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">لوحة التحكم</a></li>
                        <li class="breadcrumb-item active">خطوط السير</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->
        <div class="row filter-row">
            <div class="col-md-12 ">
                <form action="" id="filter">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="form-group form-focus">
                                <input type="date" name="day" class="form-control form-control-sm floating" @if(isset($_GET['day'])) value="{{$_GET['day'] }}"@endif>
                                <label class="focus-label">حدد اليوم </label>
                            </div>
                        </div>


                    <div class="col-sm-3 col-md-2">
                        <a href="#" onclick="$('#filter').submit()" class="btn btn-success btn-sm btn-block"> بحث </a>
                    </div>
                    @if(isset($_GET['name']) || isset($_GET['day']))
                        <div class="col-sm-3 col-md-2">
                            <a href="{{route('admin_itineraries_all')}}"  class="btn btn-link border-dark btn-sm"> مسح الفلتر  <i class="fa fa-trash"></i></a>
                        </div>
                    @endif
                    </div>
                </form>
            </div>


        </div>
        <!-- Search Filter -->

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <table
                                    class="table table-striped custom-table datatable dataTable no-footer"
                                    id="DataTables_Table_0" role="grid"
                                    aria-describedby="DataTables_Table_0_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Client ID: activate to sort column ascending"
                                            style="min-width: 200px;">وقت البدء</th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Mobile: activate to sort column ascending"
                                            style="min-width: 200px;">الحالة</th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Status: activate to sort column ascending"
                                            style="min-width: 150px;">عدد الطلبات</th>
                                        <th class="text-center sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Action: activate to sort column ascending"
                                            style="min-width: 100px;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($data))
                                    @foreach($data as  $key=>$item)
                                    <tr role="row" class="odd">
                                        <td>{{$item->created_format}}</td>
                                        <td>{{$item->status}}</td>
                                        <td>{{$item->orders()->count()}}</td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-primary" href="{{route('user_itineraries_show',$item->slug)}}"> عرض </a>
                                        </td>
                                    </tr>

                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="text-muted text-center"> لا يوجد بيانات .. </td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>

                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="DataTables_Table_0_paginate">
                                    {{$data->onEachSide(1)->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /Page Content -->

    <!-- Add Client Modal -->
    <div id="add_client" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('admin_clients_store')}}"  method="Post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label"> اسم العميل <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control  @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name')}}">
                                    @error('name')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">رقم الهاتف </label>
                                    <input class="form-control  @error('phone') is-invalid @enderror" type="text" name="phone" value="{{old('phone')}}">
                                    @error('phone')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">اضافة خريطة الموقع (خط الطول وخط العرض) <span
                                        class="text-danger">*</span></label>
                                <input class="form-control  @error('location') is-invalid @enderror" type="text" name="location" value="{{old('location')}}">
                                @error('location')
                                <small class="text-danger">{{$message}}</small>
                                @enderror

{{--                                <div class="row">--}}
{{--                                    <div class="col-md-8">--}}
{{--                                        <div class="form-group">--}}

{{--                                            <input class="form-control" type="text">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-4">--}}
{{--                                        <a href="" class="btn btn-primary">فتح الخريطة</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">المحافظة <span
                                            class="text-danger">*</span></label>
                                    <select  class="form-control floating  @error('location') is-invalid @enderror"  name="governorate_id" >
                                        @foreach(\App\Governorate::select('id','name')->get() as  $key=>$value)
                                            <option value="{{$value->id}}" @if(old('governorate_id') == $value->id) selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('governorate_id')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">عنوان تفصيلي</label>
                                    <input class="form-control  @error('address') is-invalid @enderror" type="text" name="address" value="{{old('address')}}">
                                    @error('address')
                                    <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
{{--                            <div class="col-md-6">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-form-label">Confirm Password</label>--}}
{{--                                    <input class="form-control" type="password">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                        <div class="submit-section">
                            <input type="submit" class="btn btn-primary submit-btn" value="إضافة">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add Client Modal -->

@stop
@push('footerScripts')
    <script src="{{asset('backend/assets/js/select2.min.js')}}"></script>
@endpush
