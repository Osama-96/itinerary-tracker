@extends('backend.app')
@section('title') لوحة التحكم @stop
@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">تسجيل طلب جديد</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item active">
                            بدأت هذا الخط ( {{$item->created_at->diffForHumans()}})
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 p-1">
                <form action="{{ route('store_order',auth()->user()->itineraries()->current()->first()) }}" id="orderForm" method="POST">
                    @csrf
                    <div class="card p-1">
                        <div class="card-header bg-primary text-white py-2">
                            <div class="row" >
                                <div class="col-md-12">
                                    <h3 class="my-3 text-center">حفظ طلب جديد</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body ">
                            <div class="row ">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label"> اسم العميل <span
                                                class="text-danger">*</span></label>
                                        @if(auth()->user()->governorates)
                                            <select class="form-control   @error('client_id') is-invalid @enderror" name="client_id"  id="clientSelect" required>
                                                <option value="" disabled selected>اختر العميل</option>
                                                @foreach(\Illuminate\Support\Facades\DB::table('clients')->whereIn('governorate_id',auth()->user()->governorates)->select('id','name')->get() as $key => $client)
                                                    <option value="{{$client->id}}" @if(old('client_id')) selected @endif>{{$client->name}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <select class="form-control   text-danger is-invalid " name="client_id"  id="clientSelect" required>
                                                <option value="" disabled selected > اطلب من الأدمن أن يسجل لك المحافظات الخاصة بك ليظهر لك العملاء فيها .. </option>
                                            </select>
                                            @endif

                                            </select>
                                            @error('client_id')
                                            <small class="text-danger">{{$message}}</small>
                                            @enderror
                                            <input type="text" class="form-control" name="type" hidden id="type" value="{{old('type','1')}}" >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class='row p-1' tap-switch='form'>
                                        <div class='item {{(old('type') == '1')?'active':''}} @if(!old('type')) active @endif  col-4 itemForm' data-type="1" tap='send'>
                                            <div> تسليم منتج</div>
                                            <i class='bolid'></i>
                                        </div>
                                        <div class='item {{(old('type') == '2')?'active':''}}   col-4 itemForm'  data-type="2" tap='deliver'>
                                            <div>استلام نقدية</div>
                                            <i class='bolid'></i>
                                        </div>
                                        <div class='item {{(old('type') == '3')?'active':''}}  col-4 itemForm'  data-type="3"  tap='more'>
                                            <div>اخري</div>
                                            <i class='bolid'></i>
                                        </div>
                                    </div>
                                    <div class="form-group p-1 mx-1" tap-parent='form'>
                                        <div tap-name='send' class='{{(old('type') == '1')?'active':''}} @if(!old('type')) active @endif'>
                                            @if(old('products_ids'))
                                                @foreach(old('products_ids') as $key => $val)
                                                    <div class="row field-data productsData" data-ids="" data-qty="">
                                                        <div class="col-md-6 col-sm-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label"> اسم المنتج <span class="text-danger">*</span></label>
                                                                <select class="form-control  selectProduct  @error('products_ids.'.$key) is-invalid @enderror" name="products_ids[{{$key}}]" id="select{{$key}}" data-price="showPrice{{$key}}" data-qty="qty{{$key}}" >
                                                                    <option value=""  selected>اختر المنتج</option>
                                                                    @foreach(\Illuminate\Support\Facades\DB::table('products')->select('id','name','cost')->get() as $key1=> $pro)
                                                                        <option value="{{$pro->id}}" data-price="{{$pro->cost}}" @if(old('products_ids.'.$key) ==$pro->id ) selected @endif>
                                                                            {{$pro->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('products_ids.'.$key)
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group form-group-inline">
                                                                                <div>
                                                                                    <label class="col-form-label">الكمية <span
                                                                                            class="text-danger">*</span></label>
                                                                                </div>
                                                                                <input class="form-control quantity  @error('products_qty.'.$key) is-invalid @enderror qtyProduct" type='number' step="1" min="1"    placeholder='أدخل الكمية' id="qty{{$key}}" data-select="select{{$key}}" data-price="showPrice{{$key}}"  name="products_qty[{{$key}}]"  value="{{old('products_qty.'.$key)}}" >
                                                                                @error('products_qty.'.$key)
                                                                                <small class="text-danger">{{$message}}</small>
                                                                                @enderror
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group form-group-inline">
                                                                                <div>
                                                                                    <label class="col-form-label">المبلغ </label>
                                                                                </div>

                                                                                <span class="text-primary" >
                                                                                    <input type="number"  class="subTotalInput" id="showPriceInput{{$key}}" hidden >
                                                                                    <strong id="showPrice{{$key}}" data-input="showPriceInput{{$key}}">0.00</strong>
                                                                                    ج.م
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                <div class="row x-small-padding">
                                                    <div class='col-12'>
                                                        <div class="left">
                                                            <div class="icon fa fa-plus add-field"></div>
                                                        </div>
                                                        <div class="right">
                                                            <div class="icon fa fa-trash remove-field"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="col-12">
                                                            <div class="left">
                                                                <div class="mc big-text bold">الأجمالي :  <span> <span id="productsTotal">0.00</span> ج.م</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            @else
                                                <div class="row field-data" data-ids[0]="" data-qty="[]">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="col-form-label"> اسم المنتج <span class="text-danger">*</span></label>
                                                            <select class="form-control  selectProduct @error('products_ids.0') is-invalid @enderror" name="products_ids[0]" id="select0" data-price="showPrice0" data-qty="qty0" >
                                                                <option value=""  selected>اختر المنتج</option>
                                                                @foreach(\Illuminate\Support\Facades\DB::table('products')->select('id','name','cost')->get() as $key1=> $pro)
                                                                    <option value="{{$pro->id}}" data-price="{{$pro->cost}}" @if(old('products_ids.0') ==$pro->id ) selected @endif>
                                                                        {{$pro->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">

                                                                    <div class="col-12">
                                                                        <div class="form-group form-group-inline">
                                                                            <div>
                                                                                <label class="col-form-label">الكمية <span
                                                                                        class="text-danger">*</span></label>
                                                                            </div>
                                                                            <input class="form-control @error('products_qty.0') is-invalid @enderror quantity qtyProduct" type='number' step="1" min="1"  placeholder='أدخل الكمية'  id="qty0" data-select="select0" data-price="showPrice0"   name="products_qty[0]" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="form-group form-group-inline">
                                                                            <div>
                                                                                <label class="col-form-label">المبلغ </label>
                                                                            </div>
                                                                            <span class="text-primary" >
                                                                                <input type="number" class="subTotalInput" id="showPriceInput0" hidden >
                                                                                <strong id="showPrice0" data-input="showPriceInput0">0.00</strong>
                                                                                ج.م
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row x-small-padding">
                                                    <div class='col-12'>
                                                        <div class="left">
                                                            <div class="icon fa fa-plus add-field"></div>
                                                        </div>
                                                        <div class="right">
                                                            <div class="icon fa fa-trash remove-field"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="left">
                                                            <div class="mc big-text bold">الأجمالي :  <span> <span id="productsTotal">0.00</span> ج.م</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif





                                        </div>
                                        <div tap-name='deliver' class="{{(old('type') == '2')?'active':''}} ">
                                            @if(old('received_notice'))
                                                @foreach(old('received_notice') as  $key1 => $val1)
                                                    <div class="row field-money">
                                                        <div class="col-md-6">

                                                            <div class="form-group">
                                                                <label class="col-form-label">البيان</label>
                                                                <input class="form-control  @error('received_notice.'.$key1) is-invalid @enderror" name="received_notice[{{$key1}}]" type='text' placeholder='البيان' value="{{old('received_notice.'.$key1)}}">
                                                                @error('received_notice.'.$key1)
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-form-label">المبلغ</label>
                                                                <input class="form-control moneyInput  @error('received_money.'.$key1) is-invalid @enderror" name="received_money[{{$key1}}]" type='number' step="0.1" placeholder='المبلغ' value="{{old('received_money.'.$key1)}}">
                                                                @error('received_money.'.$key1)
                                                                <small class="text-danger">{{$message}}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                <div class="row x-small-padding">
                                                    <div class='col-12'>
                                                        <div class="left">
                                                            <div class="icon fa fa-plus add-money"></div>
                                                        </div>
                                                        <div class="right">
                                                            <div class="icon fa fa-trash remove-money"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="left">
                                                            <div class="big-text bold mc">الأجمالي :   <span id="showMoney">0.00 </span>ج.م</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="row field-money">
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label class="col-form-label">البيان</label>
                                                            <input class="form-control" name="received_notice[0]" type='text' placeholder='البيان'>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-form-label">المبلغ</label>
                                                            <input class="form-control moneyInput" name="received_money[0]" type='number' step="0.1" placeholder='المبلغ'>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row x-small-padding">
                                                    <div class='col-12'>
                                                        <div class="left">
                                                            <div class="icon fa fa-plus add-money"></div>
                                                        </div>
                                                        <div class="right">
                                                            <div class="icon fa fa-trash remove-money"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="left">
                                                         <div class="big-text bold mc">الأجمالي :   <span id="showMoney">0.00 </span>ج.م</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                        <div tap-name='more' class="{{(old('type') == '3')?'active':''}} ">
                                            <div class="row mb-0">
                                                <div class="col-12 mt-3">
                                                    <p>من فضلك قم بإدخال الغرض والبيانات التي تود إرسالها.</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row" >
                                        <div class="col-12">
                                            <label class="col-form-label">ملاحظات</label>
                                            <textarea name="notes"  rows="5" class="form-control" placeholder="ملاحظات" > {{old('notes')}}</textarea>
                                        </div>
                                    </div>
                                    <div class="submit-section">

                                        <input type="button" class="btn btn-primary submit-btn" id="submitForm" value="حفظ الطلب" >
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>



    </div>
    <!-- /Page Content -->

@stop
@section('js')
    <script>
        $(document).ready(function() {
            var id;
            function addMemberField() {
                var i = $('.field-data').length + 1;
                var memberField = `
        <div class="row field-data">
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                <label class="col-form-label"> اسم المنتج <span
                        class="text-danger">*</span></label>
                <select class="form-control   selectProduct @error("products_ids.`+(i-1)+`") is-invalid @enderror"  name="products_ids[`+(i-1)+`]"  id="select`+(i-1)+`" data-price="showPrice`+(i-1)+`" data-qty="qty`+(i-1)+`"  required>
                    <option value="">اختر المنتج</option>
                    @foreach(\Illuminate\Support\Facades\DB::table("products")->select("id","name","cost")->get() as $key1=> $pro)
                <option value="{{$pro->id}}" data-price="{{$pro->cost}}" @if(old("products_ids.`+(i-1)+`") ==$pro->id ) selected @endif > {{$pro->name}}</option>
                    @endforeach
                </select>
                @error("products_ids.`+(i-1)+`")
                <small class="text-danger">{{$message}}</small>
                @enderror
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-group-inline">
                                    <div>
                                        <label class="col-form-label">الكمية <span
                                                class="text-danger">*</span></label>
                                    </div>
                                    <input class="form-control quantity  @error("products_qty.`+(i-1)+`") is-invalid @enderror qtyProduct" type='number' step="1" min="1"   placeholder='أدخل الكمية' name="products_qty[`+(i-1)+`]" id="qty`+(i-1)+`" data-select="select`+(i-1)+`" data-price="showPrice`+(i-1)+`"   value="{{old("products_qty.`+(i-1)+`")}}"  required>
                              @error("products_qty.`+(i-1)+`")
                <small class="text-danger">{{$message}}</small>
                                @enderror
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="col-md-3 col-sm-6">
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-12">
                <div class="form-group form-group-inline">
                    <div>
                        <label class="col-form-label">المبلغ </label>
                    </div>

                    <span class="text-primary" >
                        <input type="number"  class="subTotalInput" id="showPriceInput`+(i-1)+`" hidden >
                                    <strong id="showPrice`+(i-1)+`" data-input="showPriceInput`+(i-1)+`">0.00</strong>
                                    ج.م
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`;
                $('.field-data').eq($('.field-data').length - 1).after(memberField);
                $('.field-data').eq(i - 1).attr('index', i).find('.fa-times').attr('index', i);
            }

            function checkMinMembers() {
                if($('.field-data').length == 1) {
                    $('.remove-field').fadeOut();
                } else {
                    $('.remove-field').fadeIn();
                }
            }
            checkMinMembers();
            $('.add-field').on('click', function () {
                addMemberField();
                checkMinMembers();
                checkQuantity();
            });
            $('.remove-field').on('click', function () {
                // id = $(this).attr('index') - 1;
                var removeId = $('.field-data').length - 1;
                $('.field-data').eq(removeId).remove();
                var total = 0;
                $('.subTotalInput').each(function (){
                    total += parseInt($(this).val());
                });
                let products_total_span = $('#productsTotal').text(parseInt(total));
                checkMinMembers();
            });

            function addMoneyField() {
                var i = $('.field-money').length + 1;
                var moneyField = `
        <div class="row field-money">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-form-label">البيان</label>
                <input class="form-control" type='text'  name="received_notice[`+(i-1)+`]" type="text" placeholder="البيان">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-form-label">المبلغ</label>
                <input class="form-control moneyInput" name="received_money[`+(i-1)+`]" type="number" step="0.1" placeholder='المبلغ'>
            </div>
        </div>
    </div>
`;
                $('.field-money').eq($('.field-money').length - 1).after(moneyField);
                $('.field-money').eq(i - 1).attr('index', i).find('.fa-times').attr('index', i);
            }

            function checkMinMoney() {
                if($('.field-money').length == 1) {
                    $('.remove-money').fadeOut();
                } else {
                    $('.remove-money').fadeIn();
                }
            }
            checkMinMoney();
            $('.add-money').on('click', function () {
                addMoneyField();
                checkMinMoney();
            });
            $('.remove-money').on('click', function () {
                var removeId = $('.field-money').length - 1;
                $('.field-money').eq(removeId).remove();
                var total = 0 ;
                $('.moneyInput').each(function (){
                    total += parseInt($(this).val());
                });
                $('#showMoney').text(parseInt(total));
                checkMinMoney();

            });

            function checkQuantity() {
                $('.quantity').on('change', function () {
                    if ($(this).val() < 2) {
                        $(this).val(1);
                    }
                })
            }
            checkQuantity()
        });

    </script>
    <script>
        $('body').on("change",".selectProduct", function(e){ //user click on remove text
            e.preventDefault();
            let selected   = $(this).find('option:selected');
            let price      = selected.data('price');
            let qtyInput        = $(this).data('qty');
            let qty             = $('#'+qtyInput).val();
            let showPriceSpan  = $(this).data('price');
            let sub_total  = qty * price;
            let text = $('#'+showPriceSpan).text()
            $('#'+showPriceSpan).text(sub_total);
            let showPriceInput = $('#'+showPriceSpan).data('input');
            $('#'+showPriceInput).val(sub_total);
            var total = 0;
            $('.subTotalInput').each(function (){
                total += parseInt($(this).val());
            });
            let products_total_span = $('#productsTotal').text(parseInt(total));
        });
        $('body').on("change",".qtyProduct", function(e){ //user click on remove text
            e.preventDefault();

            let select     = $(this).data('select');
            let selected   = $('#'+select).find('option:selected');
            let price      = selected.data('price');

            let qty        = $(this).val();
            let showPriceSpan  = $(this).data('price');
            let sub_total  = qty * parseInt(price);

            $('#'+showPriceSpan).text(sub_total);
            let showPriceInput = $('#'+showPriceSpan).data('input');

            $('#'+showPriceInput).val(sub_total);
            var total = 0 ;

            $('.subTotalInput').each(function (){
                total += parseInt($(this).val());
            });

            let products_total_span = $('#productsTotal').text(parseInt(total));
        });

        $('body').on("change",".moneyInput", function(e){ //user click on remove text
            e.preventDefault();
            var total = 0 ;
            $('.moneyInput').each(function (){
                total += parseInt($(this).val());
            });
             $('#showMoney').text(parseInt(total));
        });
        $('.itemForm').click(function (){
            let type = $(this).data('type');
            $('#type').val(type);
        });
        $('body').on('click','#submitForm',function() {
            if (navigator.geolocation) { // Supported
                // To add PositionOptions
                return navigator.geolocation.getCurrentPosition(function(position){
                    let lat = position.coords.latitude;
                    let lng = position.coords.longitude;
                    $('form#orderForm').append(`<input type="text" value="`+lat+`" hidden name="lat"/>`);
                    $('form#orderForm').append(`<input type="text" value="`+lng+`" hidden name="lng"/>`);
                    $('form#orderForm').submit();

                });
            } else { // Not supported

                alert("من فضلك قم باستخدام متصفح آخر يسمح بالتفاط الموقع");

                return false;
            }
        });



    </script>
@stop
