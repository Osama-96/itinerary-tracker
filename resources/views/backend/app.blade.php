<!DOCTYPE html>
<html>

<!-- Mirrored from dreamguys.co.in/smarthr/dark/{{url('/')}} by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Mar 2019 19:52:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('backend/assets/img/favicon.png')}}">
    <title>{{env('APP_NAME')}} | @yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/line-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/plugins/morris/morris.css')}}">
    @stack('cssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/style.css')}}">
    @yield('css')
    <!--[if lt IE 9]>
    <script src="{{asset('backend/assets/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/respond.min.js')}}"></script>

    <![endif]-->
    <style>
        @font-face {
            font-family: kufi;
            src: url({{asset('fonts/ArbFONTS-Droid.Arabic.Kufi_DownloadSoftware.iR_.ttf')}});
        }
         h1,h2,h3,h4,h5,h6,th,td,li,span,p,.page-title,.kufi{
            font-family: kufi;
        }
    </style>
    @stack('topScripts')
</head>
<body @guest() class='account-page' @endguest>
<!-- Main Wrapper -->
<div class="main-wrapper">

    @guest()
        @yield('auth')
    @else
    <!-- Header -->
    <div class="header">

        <!-- Logo -->
        <div class="header-left">
            <a href="{{url('/')}}" class="logo">
                <img src="{{asset('backend/assets/img/logo.png')}}" width="40" height="40" alt="">
            </a>
        </div>
        <!-- /Logo -->

        <a id="toggle_btn" href="javascript:void(0);">
            <span class="bar-icon">
                <span></span>
                <span></span>
                <span></span>
            </span>
        </a>

        <!-- Header Title -->
        <div class="page-title-box">
            <h3>لوحة التحكم</h3>
        </div>
        <!-- /Header Title -->

        <a id="mobile_btn" class="mobile_btn" href="#sidebar"><i class="fa fa-bars"></i></a>

        <!-- Header Menu -->
        <ul class="nav user-menu">

            <!-- Search -->
            <li class="nav-item mx-2">
                <div class="top-nav-search">
                    <a href="javascript:void(0);" class="responsive-search">
                        <i class="fa fa-search"></i>
                    </a>
                    @role('admin')
                    <form action="{{route('filter')}}" method="POST" >@csrf
                        <input class="form-control " style="font-size: 11px" type="text" name="name" autocomplete="off" placeholder="أدخل اسم مندوب أو عميل أو منتج..">
                        <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                    </form>
                    @endrole
                </div>
            </li>
            <!-- /Search -->

            <!-- Flag -->

            <!-- /Flag -->

            <!-- Notifications -->

            <!-- /Notifications -->

            <!-- Message Notifications -->

            <!-- /Message Notifications -->

            <li class="nav-item dropdown has-arrow main-drop">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                    <span class="user-img"><img src="{{asset('backend/assets/img/profiles/avatar-21.jpg')}}" alt="">
                        <span class="status online"></span></span>
                    <span>{{auth()->user()->name}}</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{url('/')}}">الرئيسية</a>
                    <a class="dropdown-item" style="cursor: pointer" href="{{route('reset_password')}}">تغيير كلمة المرور</a>
                    <a class="dropdown-item"style="cursor: pointer"  onclick="$('#logout').submit()">تسجيل خروج</a>
                </div>
            </li>
        </ul>
        <!-- /Header Menu -->
        <form action="{{route('logout')}}" id="logout" hidden method="post">
            @csrf
        </form>
        <!-- Mobile Menu -->
        <div class="dropdown mobile-user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                    class="fa fa-ellipsis-v"></i></a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{url('/')}}">الرئيسية</a>
                <a class="dropdown-item" style="cursor: pointer" href="{{route('reset_password')}}">تغيير كلمة المرور</a>
                <a class="dropdown-item" style="cursor: pointer" onclick="$('#logout').submit()">تسجيل خروج</a>

            </div>
        </div>
        <!-- /Mobile Menu -->

    </div>
    <!-- /Header -->
    @include('backend.includes.side')

    <!-- Page Wrapper -->
    <div class="page-wrapper" style="min-height: 907px;">
        @include('backend.includes.alerts')
        @yield('content')
    </div>
    <!-- /Page Wrapper -->

    @endguest

</div>
<!-- /Main Wrapper -->

<script type="text/javascript" src="{{asset('backend/assets/js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/assets/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/assets/js/jquery.slimscroll.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/assets/plugins/morris/morris.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/assets/plugins/raphael/raphael-min.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/assets/js/chart.js')}}"></script>
@stack('footerScripts')
<script type="text/javascript" src="{{asset('backend/assets/js/app.js')}}"></script>
<script>
    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    function readURL(input,name='#imgPreview') {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function(e) {
                $(name).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('form').submit(function(){
        $(this).prop("disabled", true);
    });
</script>
@yield('js')

<div class="sidebar-overlay"></div></body>

<!-- Mirrored from dreamguys.co.in/smarthr/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Mar 2019 19:52:59 GMT -->
</html>

<!-- <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="assets/plugins/morris/morris.min.js"></script>
<script type="text/javascript" src="assets/plugins/raphael/raphael-min.js"></script>
<script type="text/javascript" src="assets/js/chart.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script> -->
