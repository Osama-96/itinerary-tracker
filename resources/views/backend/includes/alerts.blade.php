<div class="row justify-content-center">
    <div class="col-md-8">
        @if ($errors->any())
            <div >

                @foreach ($errors->all() as $error)
                    <p class="alert alert-danger my-1">{{ $error }}</p>
                @endforeach

            </div>
        @endif
        @if(session()->has('success') || session()->has('error') || session()->has('warning'))
            <div class="alert-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session()->has('error'))

                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="fa fa-ban"></i> Alert!</h5>
                                    عملية فاشلة | {{session()->get('error')}}
                                </div>
                            @endif
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="fa fa-check"></i> Alert!</h5>
                                    عملية ناجحة | {{session()->get('success')}}
                                </div>

                            @endif
                            @if(session()->has('warning'))
                                <div class="alert alert-warning alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="fa fa-exclamation-circle"></i> Alert!</h5>
                                     انتبه | {{session()->get('warning')}}
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif


    </div>
</div>
