
<!-- Sidebar -->
<div class="sidebar" id="sidebar">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 100%; height: 847px;">
        <div class="sidebar-inner slimscroll" style="overflow: hidden; width: 100%; height: 847px;">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>
                    <li class="menu-title">
                        <span>الرئيسية</span>
                    </li>
                    <li  class="@if(url()->current() == url('/')) active @endif">
                        <a href="{{url('/')}}"><i class="la la-dashboard"></i> <span> لوحة التحكم</span></a>
                    </li>
                    @role('admin')
                    <li class="@if(url()->current() == route('admin_itineraries_all')) active @endif">
                        <a href="{{route('admin_itineraries_all')}}"><i class="la la-truck"></i> <span>خطوط السير</span></a>
                    </li>
                    <li class="@if(url()->current() == route('admin_clients_all')) active @endif">
                        <a href="{{route('admin_clients_all')}}"><i class="la la-users"></i> <span>العملاء</span></a>
                    </li>
                    <li class="@if(url()->current() == route('admin_users_all')) active @endif">
                        <a href="{{route('admin_users_all')}}"><i class="la la-user"></i> <span>المندوبين</span></a>
                    </li>
                    <li class="@if(url()->current() == route('admin_categories_all')) active @endif">
                        <a href="{{route('admin_categories_all')}}"><i class="la la-list"></i> <span>الأصناف</span></a>
                    </li>
                    <li class="@if(url()->current() == route('admin_products_all')) active @endif">
                        <a href="{{route('admin_products_all')}}"><i class="la la-cubes"></i> <span>المنتجات</span></a>
                    </li>
                    <li class="@if(url()->current() == route('user_report_create')) active @endif">
                        <a href="{{route('user_report_create')}}"><i class="la la-pencil-square"></i> <span>إنشاء تقارير</span></a>
                    </li>
                    @else
                        <li class="@if(url()->current() == route('user_itineraries_all')) active @endif">
                            <a href="{{route('user_itineraries_all')}}"><i class="la la-truck"></i> <span>خطوط السير السابقة</span></a>
                        </li>
                        <li class="@if(url()->current() == route('user_products_all')) active @endif">
                            <a href="{{route('user_products_all')}}"><i class="la la-cubes"></i> <span>المنتجات</span></a>
                        </li>
                    @endrole
                </ul>
            </div>
        </div>
        <div class="slimScrollBar"
             style="background: rgb(204, 204, 204); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; left: 1px; height: 440.669px;">
        </div>
        <div class="slimScrollRail"
             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; left: 1px;">
        </div>
    </div>
</div>
<!-- /Sidebar -->
