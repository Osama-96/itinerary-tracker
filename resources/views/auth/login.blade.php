
@extends('backend.app')
@section('title') تسجيل الدخول@stop
@section('auth')
    <div class="account-content">
        <div class="container">
        <!-- Account Logo -->
            <div class="account-logo">
                <a href="{{url('/')}}"><img src="{{asset('backend/assets/img/logo.png')}}" alt="Dreamguy's Technologies"></a>
            </div>
            <!-- /Account Logo -->

            <div class="account-box">
                <div class="account-wrapper">
                    <h3 class="account-title">تسجيل الدخول</h3>
                    <p class="account-subtitle">قم بالتسجيل للوصول للوحة التحكم الخاصة بك</p>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- Account Form -->
                    <form action="{{route('login')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label>الإيميل</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label>كلمة المرور</label>
                                </div>
                                <div class="col-auto">
                                    <a class="text-muted" href="{{route('password.request')}}">
                                        نسيت كلمة المرور؟
                                    </a>
                                </div>
                            </div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group text-center">
                            <input class="btn btn-primary account-btn" type="submit" value="دخول">
                        </div>
                        <div class="account-footer">
                            <p>إذا كنت لا تملك حساباً بعد، من فضلك أطلب من مديرك تسجيلك على النظام </p>
                        </div>
                    </form>
                    <!-- /Account Form -->

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
