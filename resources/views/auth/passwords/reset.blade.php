@extends('backend.app')

@if(auth()->check())
@section('content')
@else
@section('auth')
@endif
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title"> تغيير كلمة المرور</h3>
                    <ul class="breadcrumb">

                        <li class="breadcrumb-item"><a href="{{url('/')}}"> @if(auth()->check())لوحة التحكم @else تسجيل الدخول @endif </a></li>
                        <li class="breadcrumb-item active">تغيير كلمة المرور</li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-white text-center bg-primary">
                        <span style="font-size: 20px">
                        تغيير كلمة المرور
                        </span>
                    </div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="form-group row justify-content-center">
                                <div class="col-md-6 offset-2">
                                    @foreach ($errors->all() as $error)
                                        <p class="alert alert-danger my-1">{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <form method="POST" action="{{ route('password_update',$user->id) }}">
                            @csrf

                            <input type="hidden" name="token" >

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">الاسم</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control "  value="{{ $user->name }}" required autofocus readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">الإيميل</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control "  value="{{ $user->email }}" required autocomplete="email" autofocus readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">كلمة المرور الجديدة</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">تأكيد كلمة المرور</label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                       تغيير كلمة المرور
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
