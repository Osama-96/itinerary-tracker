@extends('backend.app')
@section('auth')
    <div class="account-content">
        <div class="container">
            <!-- Account Logo -->
            <div class="account-logo">
                <a href="#"><img src="{{asset('backend/assets/img/logo.png')}}" alt="Dreamguy's Technologies"></a>
            </div>
            <div class="account-box">
                <div class="account-wrapper">
                    <h3 class="account-title">استرجاع كلمة المرور</h3>
                    <p class="account-subtitle">قم بإدخال الإيميل الخاص بك</p>
                    @if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="get" action="{{ route('check_email') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <label for="email">الإيميل</label>
                            </div>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-0 mt-3 justify-content-center">
                            <div class="col-md-6 ">
                                <button type="submit" class="btn btn-primary">
                                   التأكد من الإيميل
                                </button>
                            </div>
                            <div class="col-md-2 offset-4" >
                                <a href="{{url('/')}}" class="btn btn-outline-secondary btn-sm">عودة </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

    </div>
</div>
@endsection
