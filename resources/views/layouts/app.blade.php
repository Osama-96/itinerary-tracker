<!DOCTYPE html>
<html>

<!-- Mirrored from dreamguys.co.in/smarthr/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Mar 2019 19:52:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <title>Dashboard</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/morris/morris.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class='account-page'>
<!-- Main Wrapper -->
<div class="main-wrapper">
    <div class="account-content">
        <div class="container">

            <!-- Account Logo -->
            <div class="account-logo">
                <a href="index.html"><img src="assets/img/logo.png" alt="Dreamguy's Technologies"></a>
            </div>
            <!-- /Account Logo -->

            <div class="account-box">
                <div class="account-wrapper">
                    <h3 class="account-title">Login</h3>
                    <p class="account-subtitle">Access to our dashboard</p>

                    <!-- Account Form -->
                    <form action="index.html">
                        <div class="form-group">
                            <label>Email Address</label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label>Password</label>
                                </div>
                                <div class="col-auto">
                                    <a class="text-muted" href="forgot-password.html">
                                        Forgot password?
                                    </a>
                                </div>
                            </div>
                            <input class="form-control" type="password">
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-primary account-btn" type="submit">Login</button>
                        </div>
                        <div class="account-footer">
                            <p>Don't have an account yet? <a href="register.html">Register</a></p>
                        </div>
                    </form>
                    <!-- /Account Form -->

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Main Wrapper -->

<script type="text/javascript" src="{{asset("backend/assets/js/jquery-3.2.1.min.js")}}"></script>
<script type="text/javascript" src="{{asset("backend/assets/js/popper.min.js")}}"></script>
<script type="text/javascript" src="{{asset("backend/assets/js/bootstrap.min.js")}}"></script>
<script type="text/javascript" src="{{asset("backend/assets/js/jquery.slimscroll.js")}}"></script>
<script type="text/javascript" src="{{asset("backend/assets/plugins/morris/morris.min.js")}}"></script>
<script type="text/javascript" src="{{asset("backend/assets/plugins/raphael/raphael-min.js")}}"></script>
<script type="text/javascript" src="{{asset("backend/assets/js/chart.js")}}"></script>
<script type="text/javascript" src="{{asset("backend/assets/js/app.js")}}"></script>


<div class="sidebar-overlay"></div>
</body>

<!-- Mirrored from dreamguys.co.in/smarthr/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Mar 2019 19:52:59 GMT -->
</html>

<!-- <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="assets/plugins/morris/morris.min.js"></script>
<script type="text/javascript" src="assets/plugins/raphael/raphael-min.js"></script>
<script type="text/javascript" src="assets/js/chart.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script> -->
