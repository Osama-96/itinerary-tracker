<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Itinerary extends Model
{
    use SoftDeletes;
    use HasSlug;


    /**
     * @var array
     */
    protected $guarded = ['id'];
    /**
     * @var array
     */
    protected $casts= [
        'start_location'=>'array',
        'end_location'=>'array',
    ];
    protected $dates= [
        'start','end','created_at','updated_at'
    ];
    /**
     * @var mixed
     */

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('start')
            ->saveSlugsTo('slug');
    }

    /**
     * @return string
     */

    public function getRouteKeyName() :string
    {
        return 'slug';
    }

    public function getCreatedFormatAttribute()
    {
        $time = $this->created_at?\Carbon\Carbon::parse($this->created_at)->format('Y-m-d H:i'): null;
        return $time;
    }
    public function getEndFormatAttribute()
    {
        $time = $this->end?\Carbon\Carbon::parse($this->end)->format('Y-m-d H:i'): null;
        return $time;
    }

    public function getEndedAttribute()
    {
        return ($this->end != null)? true: false;
    }
    public function getStatusAttribute()
    {
        if(!$this->end ){
            return 'حالي';
        }else{
            return 'منتهي في: '.$this->end_format;
        }
    }
    /**
     * @param $query
     * @return mixed
     */
    public function scopeCurrent($query)
    {
        return $query->whereNull('end')->whereNull('end_location');
    }
    public function scopeEnded($query)
    {
        return $query->whereNotNull('end')->whereNotNull('end_location');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Order::class, 'itinerary_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
