<?php
namespace App\Traits;
use App\Http\Requests\ProductRequest;
trait uploadFileTrait
{
    static function uploadImage( $request, string $inputName = 'file', string $path = 'uploads', array $validation = [])
    {
        // remove any / char form var
        $path = 'uploads/' . rtrim($path, '/');
        //dd($request->all());
        // validate Image
        if (empty($validation)) $request->validate([$inputName => ['sometimes', 'mimes:jpeg,jpg,png,pdf']]);
        else $request->validate([$inputName => $validation]);


        // check has file
        if ($request->hasFile($inputName)) {
            $image = $request->file($inputName);
            $filename = uniqid('', true) . '.' . date('Y.m.d.H.i.s') . '.' . $image->getClientOriginalExtension();
            $image->move(public_path() . '/' . rtrim($path, '/'), $filename);
            return $path . '/' . $filename;
        }
        // default Image
        return null;
    }
}
