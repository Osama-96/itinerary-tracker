<?php

namespace App\Providers;

use App\Interfaces\categoryInterface;
use App\Interfaces\ClientInterface;
use App\Interfaces\OrderInterface;
use App\Interfaces\ProductInterface;
use App\Interfaces\UserInterface;
use App\Reposatories\category\categoryReposatory;
use App\Reposatories\client\ClientReposatory;
use App\Reposatories\product\ProductReposatory;
use App\Reposatories\user\UserReposatory;
use App\Reposatories\order\OrderReposatory;
use Illuminate\Support\ServiceProvider;

class ReposatoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ClientInterface::class,ClientReposatory::class);
        $this->app->bind(UserInterface::class,UserReposatory::class);
        $this->app->bind(ProductInterface::class,ProductReposatory::class);
        $this->app->bind(OrderInterface::class,OrderReposatory::class);
        $this->app->bind(categoryInterface::class,categoryReposatory::class);
    }
}
