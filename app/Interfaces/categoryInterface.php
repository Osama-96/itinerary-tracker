<?php

namespace App\Interfaces;

use App\Http\Requests\CategoryRequest;

interface categoryInterface
{
    public function all();

    public function item($slug);

    public function Store(CategoryRequest $request);

    public function Update(CategoryRequest $request, $slug);

    public function delete($slug): bool;
}
