<?php

namespace App\Interfaces;

use App\Http\Requests\StoreOrderRequest;

interface OrderInterface
{
    public function store(StoreOrderRequest $request, $slug);
}
