<?php

namespace App\Interfaces;

use App\Client;
use App\Http\Requests\ClientRequest;

interface ClientInterface
{
    public function all();

    public function item(Client $client);

    public function storeItem(ClientRequest $request);

    public function updateItem(ClientRequest $request, Client $item);

    public function deleteItem(Client $item);
}
