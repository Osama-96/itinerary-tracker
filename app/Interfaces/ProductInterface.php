<?php

namespace App\Interfaces;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;
use App\Reposatories\product\User;
use App\Traits\uploadFileTrait;
interface ProductInterface
{
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param Product $user
     * @return Product
     */
    public function item(Product $user);

    /**
     * @param ProductRequest $request
     * @return mixed
     */
    public function storeItem(ProductRequest $request);

    /**
     * @param UpdateProductRequest $request
     * @param Product $item
     * @return mixed
     */
    public function updateItem(UpdateProductRequest $request, Product $item);

    /**
     * @param Product $item
     * @return bool
     * @throws \Exception
     */
    public function deleteItem(Product $item): bool;
}
