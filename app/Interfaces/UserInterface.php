<?php

namespace App\Interfaces;

use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\User;

interface UserInterface
{
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param User $user
     * @return User
     */
    public function item(User $user);

    /**
     * @param UserRequest $request
     * @return mixed
     */
    public function storeItem(UserRequest $request);

    /**
     * @param UserRequest $request
     * @param User $item
     * @return bool
     */
    public function updateItem(UpdateUserRequest $request, User $item);

    /**
     * @param User $item
     * @return bool
     * @throws \Exception
     */
    public function deleteItem(User $item): bool;
}
