<?php


namespace App\Reposatories\order;


use App\Http\Requests\StoreOrderRequest;
use App\Interfaces\OrderInterface;
use App\Itinerary;
use App\Order;

class OrderReposatory implements OrderInterface
{

    public function store(StoreOrderRequest $request , $slug)
    {
        $itinerary = getItemBySlug(new Itinerary(), $slug);
        $data = $request->validated();
        $order = new Order();
        $order->user_id = auth()->user()->id;
        $order->client_id = $data['client_id'];
        $order->itinerary_id = $itinerary->id;
        $order->location =[
            'lat'=>$data['lat'],
            'lng'=>$data['lng'],
        ];
        $order->data = $data;
        $order->save();
        return $order;

    }
}
