<?php


namespace App\Reposatories\client;

use App\Client;
use App\Http\Requests\ClientRequest;
use App\Interfaces\ClientInterface;


class ClientReposatory implements ClientInterface
{

    public function all()
    {
        if (isset($_GET['name']))
        return Client::where('name','like', '%' . $_GET['name'] . '%')->orderByDesc('id')->paginate();

        return Client::orderByDesc('id')->paginate();
    }

    public function item(Client $client)
    {
        return $client;
    }

    public function storeItem(ClientRequest $request)
    {
        $data = $request->validated();
        $location = ['lat'=>explode(',',$data ['location'])[0],'lng'=>explode(',',$data ['location'])[1]];
        $item = new Client();
        $item -> name               = $data ['name'];
        $item -> phone              = $data ['phone'];
        $item -> governorate_id     = $data ['governorate_id'];
        $item -> address            = $data ['address'];
        $item -> location           = $location;
        $item->save();
        return $item;
    }

    public function updateItem(ClientRequest $request , Client $item)
    {
        $data = $request->validated();
        $location = ['lat'=>explode(',',$data ['location'])[0],'lng'=>explode(',',$data ['location'])[1]];
        $item -> name               = $data ['name'];
        $item -> phone              = $data ['phone'];
        $item -> governorate_id     = $data ['governorate_id'];
        $item -> address            = $data ['address'];
        $item -> location           = $location;
        $item->save();

        return $item;
    }

    public function deleteItem(Client $item)
    {
        $item->delete();
        return true;
    }

}
