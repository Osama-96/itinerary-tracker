<?php


namespace App\Reposatories\product;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Interfaces\ProductInterface;
use App\Product;
use App\Traits\uploadFileTrait;
use Illuminate\Support\Facades\File;


class ProductReposatory implements ProductInterface
{

    /**
     * @return mixed
     */
    public function all()
    {
        if (isset($_GET['name']))
            return Product::where('name','like', '%' . $_GET['name'] . '%')->orderByDesc('id')->paginate();

        return Product::orderByDesc('id')->paginate();
    }

    /**
     * @param Product $user
     * @return Product
     */
    public function item(Product $user)
    {
        return $user;
    }

    /**
     * @param ProductRequest $request
     * @return mixed
     */
    public function storeItem(ProductRequest  $request)
    {

        $image = null;
        $data = $request->validated() ;
        if($request->hasFile('image')){
            $data['image'] = uploadFileTrait::uploadImage($request,'image','products');
        }
        return Product::create($data);
    }

    /**
     * @param UpdateProductRequest $request
     * @param Product $item
     * @return bool|mixed
     */
    public function updateItem(UpdateProductRequest $request , Product $item)
    {
        $request->validate([
            'name'=>'unique:products,name,'.$item->id,
        ]);
        $data = $request->validated();
        if($request->hasFile('image')){
            File::delete($item->image);
            $data['image'] =  uploadFileTrait::uploadImage($request,'image','products');

        }

        return $item ->update($data);
    }

    /**
     * @param Product $item
     * @return bool
     * @throws \Exception
     */
    public function deleteItem(Product $item): bool
    {

        if($item->image){
            File::delete($item->image);
        }
        return $item->delete();
    }

}
