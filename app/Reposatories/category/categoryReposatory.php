<?php


namespace App\Reposatories\category;
use App\Category;
use App\Http\Requests\CategoryRequest;
use App\Interfaces\categoryInterface;

class categoryReposatory implements categoryInterface
{
    public function all()
    {
        $query = Category::latest();
        if(isset($_GET['name']) && $_GET['name']  ){
            $query->where('name','like', '%' . $_GET['name'] . '%');

        }
        return $data = $query->with(['products'])->paginate();
    }

    public function item($slug)
    {
        return getItemBySlug(new Category(),$slug);
    }

    public function Store(CategoryRequest $request)
    {
        $data = $request->validated();
        return $item= Category::create($data);
    }
    public function Update(CategoryRequest $request, $slug)
    {
        $item = $this->item($slug);
        $data = $request->validated();
        return $item->update($data);
    }

    public function delete($slug): bool
    {
     $item = $this->item($slug);
     if($item->products()->count()){
         return false;
     }
     $item->delete();
     return true;
    }
}
