<?php


namespace App\Reposatories\user;


use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\Interfaces\UserInterface;
use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserReposatory implements UserInterface
{
    /**
     * @return mixed
     */
    public function all()
    {
        return User::role('user')->orderByDesc('id')->paginate();
    }

    /**
     * @param User $user
     * @return User
     */
    public function item(User $user)
    {
        return $user;
    }

    /**
     * @param UserRequest $request
     * @return mixed
     */
    public function storeItem(UserRequest  $request)
    {
        $data = $request->validated();

        $item =  User::create($data);
        $item->assignRole('user');
        return  $item;
    }

    /**
     * @param UserRequest $request
     * @param User $item
     * @return bool
     */
    public function updateItem(UpdateUserRequest $request , User $item)
    {

        $request->validate([
            'email'=>['unique:users,email,'.$item->id],
            'personal_id'=>['unique:users,personal_id,'.$item->id],
        ]);
        $data = $request->validated();
        return $item = $item ->update($data);
    }

    /**
     * @param User $item
     * @return bool
     * @throws \Exception
     */
    public function deleteItem(User $item): bool
    {

        return $item->delete();
    }
}
