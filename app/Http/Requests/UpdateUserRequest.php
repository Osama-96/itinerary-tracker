<?php

namespace App\Http\Requests;

use App\Rules\EgyptianID;
use Axiom\Rules\TelephoneNumber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'            => ['required', 'string', 'max:255'],
            'email'           => ['required', 'string', 'email', 'max:255'],
            //'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone'           =>['required', new TelephoneNumber()],
            'personal_id'     =>['required','string','size:14' ],
            'address'         =>['required','string'],
            'startJobDate'    =>['required','date','before_or_equal:today'],
            'governorates'    =>['required','array'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'اسم المستخدم مطلوب',
            'phone.required'  => ' رقم المستخدم مطلوب',
            'email.required'  => ' إيميل المستخدم مطلوب',
            'email.required'  => ' لابد من التأكد من صحة الإيميل ',
            'email.unique'  => ' تم حفظ هذا الإيميل سابقاً بالفعل.. ',
            'email.max'  => ' الايمسل لابد ألا يزيد عن 255 حرف.. ',
            'personal_id.unique'  => ' تم حفظ هذا الرقم القومي سابقاً بالفعل.. ',
            'personal_id.required'  => ' رقم بطاقة المستخدم مطلوب',
            'personal_id.size'  => ' رقم بطاقة المستخدم لابد أن يكون 14 رقم',
            'personal_id.integer'  => ' رقم بطاقة المستخدم لابد أن يكون مكوناً من أرقام فقط',
            'before_or_equal.required'  => ' تاريخ بدء الوظيفة مطلوب',
            'before_or_equal.date'  => ' تاريخ بدء الوظيفة لابد أن يكون تاريخاً',
            'before_or_equal.before_or_equal'  => '  تاريخ بدء الوظيفة لابد أن يكون تاريخاً ( اليوم أو تاريخاً قبل ذلك)',
            'name.string'  => 'اسم المستخدم لابد أن يكون مكوناً من أحرف.. ',
            'governorates.required' => 'قم باختيار المحافظات',
            'governorates.*.integer' => 'هناك مشكلة في ايجاد Id بعض',
            'address.required' => 'عنوان العميل مطلوب',
            'address.string' => 'عنوان العميل لابد أن يكون نصاً',
        ];
    }
}
