<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangepasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'=>['required','string','min:6','confirmed'],
        ];
    }

    public function messages()
    {
        return[
            'password.required' => 'كلمة المرور مطلوبة',
            'password.min' => 'كلمة المرور لابد أن تكون مكونة من 6 أحرف أو أرقام على الأقل',
            'password.confirmed' => 'كلمة المرور غير متطابقة..',
        ];
    }
}
