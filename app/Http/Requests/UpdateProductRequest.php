<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required','max:191', 'string'],
            'cost'=>['required','min:0', 'integer'],
            'image'=>['sometimes','max:2000'],
            'image'=>['mimes:jpeg,jpg,png'],
            'category_id'=>['required','integer'],
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'اسم المنتج مطلوب',
            'name.max'=>'اسم المنتج لابد ألا يزيد عن 191 حرف',
            'name.string'=>'اسم المنتج لابد وأن يكون مكوناً من حروف',
            'name.unique'=>'لقد أدخلت هذا المنتج من قبل..',
            'cost.min'=>'السعر يجب ألا يقل عن 0 جنيه',
            'cost.integer'=>'السعر يجب أن يكون رقماً',
            'image.mimes'=>'لابد أن تكون الصورة بإحدى الصيغ التالية: [jpeg, jpg, png ]',
            'image.max'=>'لابد الا يزيد حجم الصورة عن 2 ميجا',
            'category_id.required'=>'لابد من اختيار الصنف',
            'category_id.integer'=>' لابد من اختيار الصنف',
        ];
    }
}
