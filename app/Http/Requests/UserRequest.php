<?php

namespace App\Http\Requests;

use App\Rules\EgyptianID;
use Axiom\Rules\LocationCoordinates;
use Illuminate\Foundation\Http\FormRequest;
use Axiom\Rules\TelephoneNumber;
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * +++++++++++++++++++++++++++++++
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            => ['required', 'string', 'max:255'],
            'email'           => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'        => ['required', 'string', 'min:6', 'confirmed'],
            'phone'           =>['required', new TelephoneNumber()],
            'personal_id'     =>['required','string','size:14' , new EgyptianID() , 'unique:users'],
            'address'         =>['required','string'],
            'startJobDate'    =>['required','date','before_or_equal:today'],
            'governorates'    =>['required','array'],


        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'اسم المستخدم مطلوب',
            'password.required' => 'كلمة المرور مطلوبة',
            'password.min' => 'كلمة المرور لابد أن تكون مكونة من 6 أحرف أو أرقام على الأقل',
            'password.confirmed' => 'كلمة المرور غير متطابقة..',
            'phone.required'  => ' رقم المستخدم مطلوب',
            'email.required'  => ' إيميل المستخدم مطلوب',
            'email.required'  => ' لابد من التأكد من صحة الإيميل ',
            'email.unique'  => ' تم حفظ هذا الإيميل سابقاً بالفعل.. ',
            'email.max'  => ' الايمسل لابد ألا يزيد عن 255 حرف.. ',
            'personal_id.unique'  => ' تم حفظ هذا الرقم القومي سابقاً بالفعل.. ',
            'personal_id.required'  => ' رقم بطاقة المستخدم مطلوب',
            'personal_id.size'  => ' رقم بطاقة المستخدم لابد أن يكون 14 رقم',
            'personal_id.integer'  => ' رقم بطاقة المستخدم لابد أن يكون مكوناً من أرقام فقط',
            'before_or_equal.required'  => ' تاريخ بدء الوظيفة مطلوب',
            'before_or_equal.date'  => ' تاريخ بدء الوظيفة لابد أن يكون تاريخاً',
            'before_or_equal.before_or_equal'  => '  تاريخ بدء الوظيفة لابد أن يكون تاريخاً ( اليوم أو تاريخاً قبل ذلك)',
            'name.string'  => 'اسم المستخدم لابد أن يكون مكوناً من أحرف.. ',
            'governorates.required' => 'قم باختيار المحافظات',
            'governorates.*.integer' => 'هناك مشكلة في ايجاد Id بعض',
            'address.required' => 'عنوان العميل مطلوب',
            'address.string' => 'عنوان العميل لابد أن يكون نصاً',
            'startJobDate.required' => 'تاريخ بدء الوظيفة مطلوب',
            'startJobDate.date' => 'تاريخ بدء الوظيفة لا بد أن يكون تاريخاً',
            'startJobDate.before_or_equal' => ' تاريخ بدء الوظيفة لا بد أن يكون تاريخاً سابقاً أو مساوياً لليوم',
            ];
    }
}
