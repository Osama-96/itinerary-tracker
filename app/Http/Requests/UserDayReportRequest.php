<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserDayReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'=>['required'],
            'from'=>['required','date','before_or_equal:today'],
            'to'=>['required','date','before_or_equal:today', 'after_or_equal:from'],
        ];
    }

    public function messages()
    {
        return[
        'user_id.required'=>'اختر المندوب',
        'from.required'=>'حدد تاريخ البدء',
        'to.required'=>'حدد تاريخ الانتهاء',
        'from.before_or_equal'=>'لابد أن يكون تاريخ البدء تاريخاً سابقاً أو مساوياً لتاريخ اليوم',
        'to.before_or_equal'=>'لابد أن يكون تاريخ الانتهاء تاريخاً سابقاً أو مساوياً لتاريخ اليوم',
        'to.after_or_equal'=>'لابد أن يكون تاريخ الانتهاء تاريخاً بعد تاريخ البدء',
        ];
    }
}
