<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


         $type  =(integer) $this->request->get('type');
       // dd($this->request->all());
        //return $rules;
        if($type == 1){
            return [
                'client_id'         =>['required','integer'],
                'type'              =>['required','integer'],
                'notes'             =>['nullable','string'],
                'products_ids'      =>['required','array'],
                'products_ids.*'    =>['required','integer'],
//                'products_prices.*' =>['required','numeric'],
                'products_qty.*'    =>['required','numeric'],
                'lat'               =>['required'],
                'lng'               =>['required'],
            ];
        }elseif($type == 2){
            return [
                'client_id'     =>['required','integer'],
                'type'          =>['required','integer'],
                'notes'         =>['nullable','string'],
                'received_notice'   =>['required','array'],
                'received_notice.*' =>['required','string'],
                'received_money'    =>['required','array'],
                'received_money.*'  =>['required','numeric'],
                'lat'        =>['required'],
                'lng'        =>['required'],
            ];
        }elseif($type == 3){
            return [
                'client_id'  =>['required','integer'],
                'type'       =>['required','integer'],
                'notes'      =>['required','string'],
                'lat'        =>['required'],
                'lng'        =>['required'],
            ];
        }

    }

    public function messages()
    {
        return [
            'client_id.required' =>'قم باختيار العميل..',
            'client_id.integer' =>'قم باختيار العميل..',
            'type.required' =>'قم بتحديد نوع العملية..',
            'notes.required' =>'من فضلك قم بإدخال الملاحظات..',
            'products_ids.*.required' =>'من فضلك قم باختيار المنتجات ..',
//            'products_prices.*.required' =>'من فضلك قم بإدخال أسعار المنتجات ..',
            'products_qty.*.required' =>'من فضلك قم بإدخال كميات المنتجات ..',
            'lat.required' =>'حدثت مشكلة في رفع الموقع، من فضلك حاول مرة أخرى..',
            'lng.required' =>'حدثت مشكلة في رفع الموقع، من فضلك حاول مرة أخرى..',
            'received_notice.*' =>'قم بإدخال البيان..',
            'received_money.*' =>'قم بإدخال السعر..',
        ];
    }
}
