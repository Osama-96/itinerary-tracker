<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'=>['required'],
            'from2'=>['required','date','before_or_equal:today'],
            'to2'=>['required','date','before_or_equal:today', 'after_or_equal:from2'],
        ];
    }

    public function messages()
    {
        return[
            'client_id.required'=>'اختر المندوب',
            'from2.required'=>'حدد تاريخ البدء',
            'to2.required'=>'حدد تاريخ الانتهاء',
            'from2.before_or_equal'=>'لابد أن يكون تاريخ البدء تاريخاً سابقاً أو مساوياً لتاريخ اليوم',
            'to2.before_or_equal'=>'لابد أن يكون تاريخ الانتهاء تاريخاً سابقاً أو مساوياً لتاريخ اليوم',
            'to2.after_or_equal'=>'لابد أن يكون تاريخ الانتهاء تاريخاً بعد تاريخ البدء',
        ];
    }
}
