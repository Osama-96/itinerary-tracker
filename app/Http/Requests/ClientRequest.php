<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Axiom\Rules\LocationCoordinates;
use Axiom\Rules\TelephoneNumber;
class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            =>['required','string'],
            'phone'           =>['required','string',new TelephoneNumber()],
            'governorate_id'  =>['required','integer'],
            'address'         =>['required','string'],
            'location'        =>['required',new LocationCoordinates],
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'اسم العميل مطلوب',
            'phone.required'  => ' رقم العميل مطلوب',
            'name.string'  => 'اسم العميل لابد أن يكون مكوناً من أحرف.. ',
            'phone.numeric'  => 'رقم العميل لابد أن يكون مكوناً من أرقام.. ',
            'phone.size'  => 'رقم العميل يجب أن يكون مكونا من 11 رقماً .. ',
            'phone.not_regex'  => 'رقم العميل لابد أن لا يكون به أي حرف.. ',
            'governorate_id.required' => 'قم باختيار المحافظة',
            'governorate_id.integer' => 'هناك مشكلة في ايجاد Id المحافظة',
            'address.required' => 'عنوان العميل مطلوب',
            'address.string' => 'عنوان العميل لابد أن يكون نصاً',
            'location.required' => 'أدخل موقع العميل من خلال خط الطول والعرض من خرائط جوجل مفصول بينهما بهذه الفاصلة( , ) ..',
            'location.numeric' => 'أدخل موقع العميل من خلال خط الطول والعرض من خرائط جوجل مفصول بينهما بهذه الفاصلة( , ) ..',
        ];
    }


}
