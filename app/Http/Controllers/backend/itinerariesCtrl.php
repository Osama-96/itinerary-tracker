<?php

namespace App\Http\Controllers\backend;

use App\Itinerary;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class itinerariesCtrl extends Controller
{
    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private function user()
    {
        return auth()->user();
    }

    /**
     * @param $name
     * @return string
     */
    private function getPath($name) :string
    {
        return 'backend/pages/itineraries/'.$name;
    }
    public function index()
    {
        $query = Itinerary::latest()->with(['orders','user']);
        if(isset($_GET['name']) && $_GET['name']  ){
            $users_id = User::where('name','like', '%' . $_GET['name'] . '%')->pluck('id')->toArray();
            $query->whereIn('user_id',$users_id );
        }
        if(isset($_GET['day']) && $_GET['day']){
            $query->whereDate('created_at',$_GET['day']);
        }
        $data = $query->paginate();
        return view($this->getPath('index'),compact('data'));
    }
    public function show($slug)
    {
        $item= getItemBySlug(new Itinerary(),$slug);
        if(!$item->end) $distance = 0; else $distance = $item->distance;

        if(($item->end && !$item->distance) || !$item->end){
            $locations = $this->item_locations($item, $item->end_location);
            $item->distance = getTomTomDistatnce($locations);
            $item->save();
            $distance= $item->distance;
        }
        return view($this->getPath('show'),compact('item','distance'));
    }

    public function allOfUser()
    {
        $query = auth()->user()->itineraries()->ended()->latest()->with(['orders']);

        if(isset($_GET['day']) && $_GET['day']){
            $query->whereDate('created_at',$_GET['day']);
        }
        $data = $query->paginate();
        return view($this->getPath('user_index'),compact('data'));
    }
    public function itemOfUser($slug)
    {
        $item= getItemBySlug(new Itinerary(),$slug);
        return view($this->getPath('user_show'),compact('item'));
    }

    /**
     *
     */
    public function start(Request $request): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validate([
            'lat'=>['string'],
            'lng'=>['string'],
        ]);


        $line= $this->user()->itineraries()->create([
            'start'=>now(),
            'start_location'=>[
                'lat'=>$data['lat'],
                'lng'=>$data['lng'],
            ],
        ]);
        if($line){
            alert('success','تم بدء خط السير بنجاح.. قم بتسجيل الطلبيات في موقع العميل ثم قم بإنهاء خط السير');
        }else{
            alert('error','حدثت مشكلة ما، من فضلك حاول مرة أخرى');

        }
        return redirect()->to(url('/'));
    }
    public function end(Request $request,   $slug)
    {
        $data = $request->validate([
            'lat'=>['string'],
            'lng'=>['string'],
        ]);
        $end_location = [
            'lat'=>$data['lat'],
            'lng'=>$data['lng'],
        ];

        // get the item;
        $itinerary = getItemBySlug(new Itinerary(),$slug);

        // get the all locations of it and its orders;
        $locations = $this->item_locations($itinerary, $end_location);
        $distance = getTomTomDistatnce($locations);

        $itinerary->end = now();
        $itinerary->end_location=$end_location;
        $itinerary->distance=$distance;
        $itinerary->save();
        if($itinerary){
            alert('success','تم انهاء خط السير بنجاح.. ');
        }else{
            alert('error','حدثت مشكلة ما، من فضلك حاول مرة أخرى');

        }
        return redirect()->to(url('/'));
    }

    private function item_locations(Itinerary $itinerary, $end_location) {

        $locations [0]= $itinerary->start_location; // [ lat : 00000 , 'lng':'000000']
        $i = 1;

        if($itinerary->orders()->count()){
            foreach ($itinerary->orders()->pluck('location')->toArray() as $key=>$location){
                $locations[$i] = $location;
                $i++;
            }
        }
        if($end_location){
            $locations[$i] = $end_location;
        }
        return $locations;
    }
}
