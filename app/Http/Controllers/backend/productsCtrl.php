<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\ProductRequest;
use App\Interfaces\ProductInterface;
use App\Product;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Controllers\Controller;

class productsCtrl extends Controller
{
    /**
     * @var ProductInterface
     */
    private $productReposatory;

    /**
     * productsCtrl constructor.
     * @param ProductInterface $productReposatory
     */
    public function __construct(ProductInterface $productReposatory )
    {
        $this->productReposatory = $productReposatory;
    }

    /**
     * @param $name
     * @return string
     */
    private function getPath($name){
        return 'backend/pages/products/'.$name;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $data = $this->productReposatory->all();
        return view($this->getPath('index'),compact('data'));
    }

    public function user_products_all()
    {

        $data = $this->productReposatory->all();
        return view($this->getPath('user_index'),compact('data'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        return view($this->getPath('create'));
    }

    /**
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest  $request)
    {

        $item = $this->productReposatory->storeItem($request);
        if ($item){
            alert('success','تم حفظ المنتج بنجاح');
            return redirect()->route('admin_products_show',$item);
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $item = $this->productReposatory->item(getItemBySlug(new Product(), $slug));
        return view($this->getPath('show'),compact('item'));
    }


    /**
     * @param $slug
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($slug)
    {
        $item = $this->clientInterface->item(getItemBySlug(new Product(), $slug));
        return view($this->getPath('edit'),compact('item'));
    }

    /**
     * @param UpdateProductRequest $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProductRequest $request, $slug)
    {
        $item = $this->productReposatory->updateItem($request,getItemBySlug(new Product(), $slug));
        if ($item){
            alert('success','تم تحديث بيانات المنتج بنجاح');
            return redirect()->route('admin_products_all');
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }
    }

    /**
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($slug)
    {
        $item = $this->productReposatory->deleteItem(getItemBySlug(new Product(), $slug));
        if ($item){
            alert('success','تم حذف بيانات المنتج بنجاح');
            return redirect()->route('admin_products_all');
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }
    }
}
