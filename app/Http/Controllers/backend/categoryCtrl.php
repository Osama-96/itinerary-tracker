<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\CategoryRequest;
use App\Interfaces\categoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class categoryCtrl extends Controller
{

    /**
     * @var categoryInterface
     */
    private $category;

    public function __construct(categoryInterface $category)
    {
        $this->category = $category;
    }

    /**
     * @param $name
     * @return string
     */
    private function getPath($name) : string
    {
        return 'backend/pages/categories/'.$name;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->getPath('index'),['data'=>$this->category->all()]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        if($item = $this->category->store($request)){
            alert('success','تم حفظ البيانات بنجاح');
        }else{
            alert('error','حدثت مشكلة ما .. أعد المحاولة مرة أخرى.');
            return  back();
        }
        return redirect()->route('admin_categories_all');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return view($this->getPath('show'),['item'=> $this->category->item($slug)]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $slug)
    {
        if($item = $this->category->Update($request,$slug)){
            alert('success','تم تحديث البيانات بنجاح..');
        }else{
            alert('error','حدثت مشكلة ما .. أعد المحاولة مرة أخرى.');
            return  back();
        }
        return redirect()->route('admin_categories_all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        if($this->category->delete($slug)){
            alert('success','تم حذف العنصر بنجاح..');
        }else{
            alert('error','حدثت مشكلة ما ..تأكد من حذف كل المنتجات تحت هذا الصنف ثم أعد المحاولة مرة أخرى.');
            return  back();
        }
        return redirect()->route('admin_categories_all');
    }
}
