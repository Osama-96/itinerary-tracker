<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Interfaces\UserInterface;
use App\Reposatories\user\UserReposatory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class usersCtrl extends Controller
{
    /**
     * @var UserInterface
     */
    private $userReposatory;

    /**
     * usersCrtl constructor.
     * @param UserInterface $userReposatory
     */
    public function __construct(UserInterface $userReposatory )
    {
        $this->userReposatory = $userReposatory;
    }

    /**
     * @param $name
     * @return string
     */
    private function getPath($name):string
    {
        return 'backend/pages/users/'.$name;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = $this->userReposatory->all();

        return view('backend.pages.users.index')->with('data',$data);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view($this->getPath('create'));
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        $item = $this->userReposatory->storeItem($request);
        if ($item){

            alert('success','تم حفظ المندوب بنجاح');
            return redirect()->route('admin_users_show',$item);
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $item = $this->userReposatory->item(getItemBySlug(new User(), $slug));
        return view($this->getPath('show'),compact('item'));
    }


    /**
     * @param $slug
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($slug)
    {
        $item = $this->clientInterface->item(getItemBySlug(new User(), $slug));
        return view($this->getPath('edit'),compact('item'));
    }

    /**
     * @param UpdateUserRequest $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, $slug)
    {
        $item = $this->userReposatory->updateItem($request,getItemBySlug(new User(), $slug));

        if ($item){
            alert('success','تم تحديث بيانات المندوب بنجاح');
            return redirect()->route('admin_users_all');
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }

    }

    /**
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($slug)
    {
        $item = $this->userReposatory->deleteItem(getItemBySlug(new User(), $slug));
        if ($item){
            alert('success','تم حذف بيانات المندوب بنجاح');
            return redirect()->route('admin_users_all');
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }
    }
}
