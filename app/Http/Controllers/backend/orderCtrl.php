<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\StoreOrderRequest;
use App\Interfaces\OrderInterface;
use App\Itinerary;
use App\Reposatories\order\OrderReposatory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class orderCtrl extends Controller
{
    /**
     * @var OrderInterface
     */
    private $orderInterface;

    public function __construct(OrderInterface $orderInterface)
    {

        $this->orderInterface = $orderInterface;
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function create( $slug)
    {
        $item = getItemBySlug(new Itinerary(), $slug);
        return view('backend.pages.order',compact('item'));
    }
    public function store(StoreOrderRequest $request , $slug): \Illuminate\Http\RedirectResponse
    {
        if($this->orderInterface->store($request,$slug)){
            alert('success','تم حفظ الطلب بنجاح.. يمكنك تسجيل طلب آخر أو إنهاء خط السير الآن');
        }else {
            alert('error','حدث خطأ ما، من فضلك قم بالمحاولة في وقت لاحق..');
        }
        return redirect()->to(url('/'));
    }
}
