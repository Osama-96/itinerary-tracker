<?php

namespace App\Http\Controllers\backend;

use App\Client;
use App\Http\Requests\ClientRequest;
use App\Interfaces\ClientInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function GuzzleHttp\Promise\all;

class clientsCtrl extends Controller
{
    /**
     * clientsCtrl constructor.
     * @param ClientInterface $clientInterface
     */

    private $clientInterface;
    public function __construct(ClientInterface $clientInterface )
    {
        $this->clientInterface = $clientInterface;
    }
    private function getPath($name){
        return 'backend/pages/clients/'.$name;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->clientInterface->all();
        return view($this->getPath('index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->getPath('create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        $item = $this->clientInterface->storeItem($request);
        if ($item){
            alert('success','تم حفظ العميل بنجاح');
            return redirect()->route('admin_clients_show',$item);
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $item = $this->clientInterface->item(getItemBySlug(new Client(), $slug));
        return view($this->getPath('show'),compact('item'));
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($slug)
    {
        $item = getItemBySlug(new Client(),$slug);
        return view($this->getPath('edit'),compact('item'));
    }

    /**
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ClientRequest $request, $slug)
    {
        $item = $this->clientInterface->updateItem($request , getItemBySlug(new Client(), $slug));
        if ($item){
            alert('success','تم تحديث بيانات العميل بنجاح');
            return  redirect()->route('admin_clients_all');
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }

    }

    /**
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($slug)
    {
        $item = $this->clientInterface->deleteItem(getItemBySlug(new Client(), $slug));
        if ($item){
            alert('success','تم حذف بيانات العميل بنجاح');
            return redirect()->route('admin_clients_all');
        }else{
            alert('error','حدث خطأ ما..');
            return back();
        }

    }
}
