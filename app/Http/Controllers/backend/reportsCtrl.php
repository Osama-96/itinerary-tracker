<?php

namespace App\Http\Controllers\backend;

use App\Client;
use App\Http\Requests\ClientReportRequest;
use App\Http\Requests\UserDayReportRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class reportsCtrl extends Controller
{
    public function create()
    {
        return view('backend.pages.reports.create');
    }
    public function user_day_report(UserDayReportRequest $request)
    {
        $validated = $request->validated();
        $user = User::findOrFail($validated['user_id']);
        $from = $validated['from'];
        $to = $validated['to'];
        $data =$user->itineraries()->whereDate('created_at','>=',$from)->whereDate('created_at','<=',$to)->orderByDesc('id')->paginate();
        $distance =$user->itineraries()->whereDate('created_at','>=',$from)->whereDate('created_at','<=',$to)->sum('distance');
        return view('backend.pages.reports.index', compact('data','user','from','to','distance'));

    }
    public function client_report(ClientReportRequest $request)
    {
        $validated = $request->validated();
        $from = $validated['from2'];
        $to = $validated['to2'];
        $client = Client::findOrFail($validated['client_id']);
        $data = $client->orders()->whereDate('created_at','>=',$from)->whereDate('created_at','<=',$to)->orderByDesc('id')->paginate();
        return view('backend.pages.reports.index', compact('data','from','to','client'));
    }
}
