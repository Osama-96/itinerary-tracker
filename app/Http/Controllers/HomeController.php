<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\FilterRequest;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function filter(FilterRequest $request)
    {
        $v = $request->validated();
        $name = $v['name'];
        $users = User::where('name','like', '%' . $name . '%')->select('id','name','slug')->get();
        $products = Product::where('name','like', '%' . $name . '%')->select('id','name','slug')->get();
        $clients = Client::where('name','like', '%' . $name . '%')->select('id','name','slug')->get();
        return view('backend.pages.filter',compact('users','products','clients'));
    }
}
