<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Bodunde\GoogleGeocoder\Geocoder;
use FarhanWazir\GoogleMaps\GMaps;

class mapController extends Controller
{
    public function getDestance(Geocoder $geocoder, Request $request)
    {

        $data= $request->validate([
            'lat'=>'required',
            'lang'=>'required',
        ]);
        // get coordinates
        $coordinates = $geocoder->getCoordinates('55 Moleye Street, Yaba');


        $address = $geocoder->getAddress(6.5349646,3.3892894);

        // get distance between two locations
        $location1 = $data;
        $location2 =$data;
        $distance = $geocoder->getDistanceBetween($location1, $location2);

        dd($distance);
        /** geocoder can also be instantiated normally without DI */
        //e.g $geocoder = new Geocoder;
    }
    public function viewMap(Geocoder $geocoder)
    {
        if (isset($_GET['lng'] ) && isset($_GET['lat'])){
            $data=[
                'lat'=>$_GET['lat'],
                'lng'=>$_GET['lng'],
            ];
            $coordinates = $geocoder->getCoordinates('55 Moleye Street, Yaba');


            $address = $geocoder->getAddress(6.5349646,3.3892894);

            // get distance between two locations
            $location1 = $data;
            $location2 =[
                'lat'=>30.019878195684942,
                'lng'=>31.298993262328537
            ];
            $distance = $geocoder->getDistanceBetween($location1, $location2);
            dd($distance);
        }else{
            return view('map');
        }

    }
}
