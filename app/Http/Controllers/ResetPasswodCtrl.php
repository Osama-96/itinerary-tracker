<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangepasswordRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ResetPasswodCtrl extends Controller
{
    public function password_update(ChangepasswordRequest $request,  $id)
    {
        $user = User::where('id',$id)->first();
        $data = $request->validated();
        $pass = Hash::make($data['password']);
        $user->password = $pass;
        $user->save();
        alert('success','تم تغيير كلمة المرور بنجاح..');
        session()->flash('status','هذا الإيميل غير محفوظ على النظام..');

        return redirect()->to(url('/'));
    }

    public function check_email(Request $request)
    {
        $data=$request->validate([
            'email'=>['required','email'],
        ]);
        if(User::where('email',$data['email'])->count()){
            $user = User::where('email',$data['email'])->first();
            return view('auth.passwords.reset',compact('user'));
        }else{
            session()->flash('status','هذا الإيميل غير محفوظ على النظام..');
            return back();
        }
    }
}
