<?php
use Bodunde\GoogleGeocoder\Geocoder;
use \Illuminate\Support\Facades\DB;
use \App\Http\Requests\ProductRequest;
function getItemBySlug( $model , string $slug)
{

    return $model->where('slug',$slug)->firstOrFail();
}

function alert(string $type , string $message)
{
    session()->flash($type , $message);
}

function getGovernorateName($id){
    return (DB::table('governorates')->where('id',$id)->first())? DB::table('governorates')->where('id',$id)->first()->name: 'لم يسجل له محافظات';
}
function getDestance($one, $tow)
{
    $geocoder = new Geocoder();
//    // get distance between two locations
    return $geocoder->getDistanceBetween($one, $tow);
}

function get_web_page( $url, $cookiesIn = '' ){
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => true,     //return headers in addition to content
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLINFO_HEADER_OUT    => true,
        CURLOPT_SSL_VERIFYPEER => true,     // Validate SSL Cert
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_COOKIE         => $cookiesIn
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
    $rough_content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all($pattern, $header_content, $matches);
    $cookiesOut = implode("; ", $matches['cookie']);

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['headers']  = $header_content;
    $header['content'] = $body_content;
    $header['cookies'] = $cookiesOut;
    return $header;
}

function getTomTomDistatnce($locations,$dd = false)
{

    foreach($locations as $key=>$item) {
        $locs[$key] = trim($item['lat']).','.trim($item['lng']);
    }

    $string= implode(':',$locs);

    $url= 'https://api.tomtom.com/routing/1/calculateRoute/'.$string.'/json?key='.config('maps.tomtom_key');

    $distance = null;
    if( ini_get('allow_url_fopen') ) {
        if($data= json_decode(file_get_contents($url)) )
            $distance = (float) $data->routes[0]->summary->lengthInMeters /1000;
    } else {


        $file= get_web_page($url);


        if(isset($file['content']) && $file['content'] ){
            $data = json_decode($file['content']);
            if(!isset($data->error) ){
                if( isset($data->routes) && $data->routes){
                    $distance= (float) $data->routes[0]->summary->lengthInMeters /1000;

                }else{
                    $distance = 0;
                    alert('error','لم يستطع النظام تحديد الطريق بين المواقع المرسلة.. حاول مرة أخرى');

                }

            }else{
                $distance = 0;
            }

        }else{
            $distance = null;
        }
    }
    //dd($distance,$url);

    return $distance;

}
function getAllowFarIndex($distance){
    if($distance > config('maps.allow_far')){
        return [
            'class'=>"text-danger",
            'text'=>'كان على بعد ('.$distance.' كيلو متر ) من موقع العميل..'
        ];
    }else{
        return [
            'class'=>"text-success",
            'text'=>'وصل إلى العميل ',
        ];
    }
}

function getLineDestance( $locations )
{
    $geocoder = new Geocoder();
//    // get distance between two locations
    $distance = 0;
    if(count($locations)) {
        foreach ($locations as $key => $loc) {
            if ($key > 0) {
                if($loc) {
                    $geocoder = new Geocoder();
                    $distance += $geocoder->getDistanceBetween($locations[$key], $locations[$key - 1]);
                }
            }
        }
    }
    return $distance;
}
function getAddress($location){
    $geocoder = new Geocoder();
    return $geocoder->getAddress($location['lng'],$location['lat']);
}
function getItemById(string $table,  int $id){
    return DB::table($table)->where('id',$id)->first();
}
