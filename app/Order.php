<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
class Order extends Model
{
    use SoftDeletes;
    use HasSlug;


    /**
     * @var array
     */
    protected $guarded = ['id'];
    /**
     * @var array
     */
    protected $casts= [
        'location'=>'array',
        'data'=>'array',
    ];

    public function getTextTypeAttribute()
    {
        if((integer)$this->data['type'] == 1 ){
            return 'تسليم منتج';
        }elseif((integer)$this->data['type'] == 2){
            return ' استلام نقدية';
        }else{
            return ' عملية لغرض آخر';
        }
    }

    /**
     * @return SlugOptions
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('created_at')
            ->saveSlugsTo('slug');
    }

    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class,'client_id');
    }
}
