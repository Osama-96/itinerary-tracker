<?php

use Illuminate\Database\Seeder;
use App\User;
use \Illuminate\Support\Facades\Hash;
class adminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->truncate();
        $admin1 =  User::create([
            'name'=>'Admin 1',
            'email'=>'admin@app.com',
            'password'=>Hash::make(123456789),
        ]);
        $admin1->assignRole('admin');
        $user1 =  User::create([
            'name'=>'User 1',
            'email'=>'user@app.com',
            'slug'=>\Illuminate\Support\Str::slug('user@app.com') ,
            'password'=>Hash::make(123456789),
        ]);
        $user1->assignRole('user');
    }
}
