<?php

use Illuminate\Database\Seeder;
use App\Itinerary;
class ItinerarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('itineraries')->truncate();
        factory(Itinerary::class,50)->create();
    }
}
