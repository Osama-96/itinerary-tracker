<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Product;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->truncate();
        foreach (DB::table('categories')->select('id')->get() as $key=>$category) {
            factory(Product::class, 4)->create([
                'category_id' => $category->id
            ]);
        }
    }
}
