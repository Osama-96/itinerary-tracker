<?php

use Illuminate\Database\Seeder;

class GovernorateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('governorates')->truncate();
        \DB::table('clients')->truncate();
        foreach (config('places') as $key => $place){

            $governorate = \App\Governorate::create(['name'=>$place[0]]);
             factory(\App\Client::class, 5)->create(['governorate_id' =>$governorate->id]);
        }
    }
}
