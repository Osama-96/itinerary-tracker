<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Itinerary;
use Faker\Generator as Faker;
use Carbon\Carbon;
$factory->define(Itinerary::class, function (Faker $faker) {
    return [
        'user_id'=> \App\User::select('id') ->inRandomOrder()->first()->id,
        'slug'=> $faker->slug,
        'start'=>$faker->dateTimeBetween( Carbon::now()->subMonth(), now()),
        'start_location'=>[
            'lat'=>$faker->latitude,
            'lng'=>$faker->longitude,
        ],
        'end'=>$faker->dateTimeBetween( Carbon::now()->subWeek(), now()),
        'end_location'=>[
            'lat'=>$faker->latitude,
            'lng'=>$faker->longitude,
        ],
    ];
});
