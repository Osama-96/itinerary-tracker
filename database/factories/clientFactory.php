<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'phone'=>$faker->phoneNumber,
        'address'=>$faker->address,
        'location'=>['lat'=>$faker->latitude , 'lng'=>$faker->longitude],
        'slug'=>$faker->unique()->slug,
    ];
});
